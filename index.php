<?php include 'core/config.php'; ?>
<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <title>KiMoMon | Log in</title>

	  <!-- Google Font: Source Sans Pro -->
	  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	  <!-- Font Awesome -->
	  <link rel="stylesheet" href="assets/plugins/fontawesome-free/css/all.min.css">
	  <!-- icheck bootstrap -->
	  <link rel="stylesheet" href="assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	  <!-- Theme style -->
	  <link rel="stylesheet" href="assets/dist/css/adminlte.min.css">
	  <style type="text/css">
	  	.login-page, .register-page {
	  		justify-content: unset !important;
	  		-webkit-justify-content: unset !important;
	  		padding-top: 8%;
	  	}
	  </style>
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
		  <!-- /.login-logo -->
		  <div class="card card-outline card-primary">
		    <div class="card-header text-center">
		      <a href="index.php" class="h1"><b>Ki</b>MoMon</a>
		    </div>
		    <div class="card-body">
		      <p class="login-box-msg">Kinder Module Monitoring System</p>

		      <form id="formLogin" action="" method="post">
		        <div class="input-group mb-3">
		          <input type="Email" class="form-control" placeholder="Email" name="uname">
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <span class="fas fa-envelope"></span>
		            </div>
		          </div>
		        </div>
		        <div class="input-group mb-3">
		          <input type="password" class="form-control" placeholder="Password" name="pass">
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <span class="fas fa-lock"></span>
		            </div>
		          </div>
		        </div>
		        <div class="row">
		          <!-- /.col -->
		          <div class="col-4 offset-8">
		            <button type="submit" class="btn btn-primary btn-block btn-signin">Sign In</button>
		          </div>
		          <!-- /.col -->
		        </div>
		      </form>

		      <!-- <p class="mb-1">
		        <a href="#">I forgot my password</a>
		      </p> -->
		      <p class="mb-0">
		        <a href="signup.php" class="text-center">Register a new membership</a>
		      </p>
		    </div>
		    <!-- /.card-body -->
		  </div>
		  <!-- /.card -->
		</div>
		<!-- /.login-box -->

	<!-- jQuery -->
	<script src="assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="assets/dist/js/adminlte.min.js"></script>
	</body>
</html>

<script type="text/javascript">
	$("#formLogin").submit( function(e){
		e.preventDefault();
		$(".btn-signin").prop("disabled", true);

		var data = $(this).serialize();
		setTimeout( function(){
			$.ajax({
				type: "POST",
				url: "ajax/auth.php",
				data: data,
				success: function(data){
					if(data == 1){
						window.location="main/index.php?page=<?=page_url('dashboard')?>";
					}else{
						alert("Error: Username or password incorrect.");
						$(".btn-signin").prop("disabled", false);
						$(".btn-signin").html("Sign in");
					}
				}
			});
		},2000);
	});
</script>