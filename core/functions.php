<?php

	function page_url($page){
		return md5(base64_encode($page));
	}

	function enCrypt($data){
		return base64_encode($data);
	}

	function deCrypt($data){
		return base64_decode($data);
	}

	function getSchoolName($conn, $uID){
		$data = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM tbl_users WHERE user_id = '$uID'"));
		$sData = mysqli_fetch_array(mysqli_query($conn,"SELECT school_name FROM tbl_schools WHERE school_id = '$data[school_id]'"));

		return strtoupper($sData[0]);
	}

	function getTeacherName($conn, $uID){
		$data = mysqli_fetch_array(mysqli_query($conn,"SELECT name FROM tbl_users WHERE user_id = '$uID'"));

		return strtoupper($data[0]);
	}

	function getStudentName($conn, $uID){
		$data = mysqli_fetch_array(mysqli_query($conn,"SELECT name FROM tbl_users WHERE user_id = '$uID'"));

		return strtoupper($data[0]);
	}

	function getClassName($conn, $cID){
		$data = mysqli_fetch_array(mysqli_query($conn,"SELECT class_name FROM tbl_classes WHERE class_id = '$cID'"));

		return isset($data[0])?strtoupper($data[0]):"N.A.";
	}

	function getSubjectName($conn, $sID){
		$data = mysqli_fetch_array(mysqli_query($conn,"SELECT subject_name FROM tbl_subject WHERE subject_id = '$sID'"));

		return isset($data[0])?strtoupper($data[0]):null;
	}

	function hasAnswered($conn, $mID, $uID){
		$data = mysqli_num_rows(mysqli_query($conn,"SELECT s_answer_id FROM tbl_student_answers WHERE user_id = '$uID' AND module_id = '$mID'"));

		return $data;
	}

	function isPrintable($conn, $mID, $uID){
		$data = mysqli_fetch_array(mysqli_query($conn,"SELECT mq_id, ma_id FROM tbl_student_answers WHERE user_id = '$uID' AND module_id = '$mID'"));

		return isset($data[0])&&$data[0]==0&&isset($data[1])&&$data[1]==0?1:0;
	}

?>