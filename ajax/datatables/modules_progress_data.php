<?php
	session_start();
	include '../../core/config.php';
	$date_now = date("Y-m-d");
	$data = mysqli_query($conn,"SELECT * FROM tbl_modules m INNER JOIN tbl_subject s ON m.subject_id = s.subject_id INNER JOIN tbl_classes_student cs ON cs.class_id = s.class_id WHERE cs.added_by = '$_SESSION[uid]' AND (m.content_type = 1 OR m.content_type = 3) AND m.module_deadline >= '$date_now' AND m.is_posted = 1 GROUP BY m.module_id");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$ans_status = hasAnswered($conn, $row["module_id"], $_SESSION['uid']);
		$status = date("Y-m-d", strtotime($row["module_deadline"])) < date("Y-m-d")?"Expired":($ans_status == 0?"Incomplete":"Completed");

		$list = array();
		$list["count"] = $count++;
		$list["module_id"] = $row["module_id"];
		$list["module_name"] = strtoupper($row["module_name"]);
		$list["module_deadline"] = $row["content_type"] == 1 || $row["content_type"] == 3?date("F d, Y", strtotime($row["module_deadline"])):"N/A";
		$list["date_added"] = $row["date_added"];
		$list["c_type"] = $row["content_type"] != 2 ?"Answer":"View";
		$list["content_type"] = $row["content_type"];
		$list["type"] = $row["content_type"] == 1?"Module":($row["content_type"] == 2?"Material":"Activity");
		$list["disable"] = date("Y-m-d", strtotime($row["module_deadline"])) < date("Y-m-d")?2:($ans_status == 0?0:1);
		$list["status"] = $row["content_type"] == 2?"N/A":$status;
		$list["c_id"] = $row["class_id"];
		$list["s_id"] = $row["subject_id"];

		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>