<?php
	session_start();
	include '../../core/config.php';

    if($_SESSION["role"] == 0){
        $data = mysqli_query($conn,"SELECT * FROM tbl_lesson l INNER JOIN tbl_users u ON u.user_id = l.added_by INNER JOIN tbl_classes c ON c.added_by = u.user_id INNER JOIN tbl_classes_student cs ON c.class_id = cs.class_id WHERE cs.added_by = '$_SESSION[uid]'");
    }else{
        $data = mysqli_query($conn,"SELECT * FROM tbl_lesson WHERE added_by = '$_SESSION[uid]'");
    }

	$response["data"] = array();
	while($row = mysqli_fetch_array($data)){
        $filename = explode("/", $row["lesson_data"]);
		$list = array();
		$list["lesson_id"] = $row["lesson_id"];
		$list["lesson_name"] = $row["lesson_name"];
		$list["lesson_data"] = $row["lesson_type"] == 1?"<a href='".$row["lesson_data"]."' target='_blank'>".$row["lesson_data"]."</a>":"<a href='".$row["lesson_data"]."' target='_blank' download>".$filename[3]."</a>";
		$list["class"] = getClassName($conn,$row["class_id"]) != ""?getClassName($conn,$row["class_id"]):"Removed";
		$list["subject"] = getSubjectName($conn,$row["subject_id"]) != ""?getSubjectName($conn,$row["subject_id"]):"Removed";
		$list["type"] = $row["lesson_type"] == 1?"Link":"File";
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>