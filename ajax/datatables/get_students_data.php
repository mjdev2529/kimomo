<?php
	session_start();
	include '../../core/config.php';
	$cs_data = explode("-", $_POST["c_id"]);
	$c_id = $cs_data[0];
	$s_id = $cs_data[1];

	$data = mysqli_query($conn,"SELECT *, cs.added_by as student FROM tbl_classes_student cs JOIN tbl_classes c ON cs.class_id = c.class_id AND cs.class_code = c.class_code INNER JOIN tbl_subject s ON s.class_id = c.class_id WHERE c.added_by = '$_SESSION[uid]' AND c.class_id = '$c_id' AND s.subject_id = '$s_id'");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){

		$list = array();
		$list["count"] = $count++;
		$list["sclass_id"] = $row["sclass_id"];
		$list["class_id"] = $row["class_id"];
		$list["subject_id"] = $row["subject_id"];
		$list["student_id"] = $row["student"];
		$list["student_name"] = strtoupper(getStudentName($conn, $row["student"]));
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>