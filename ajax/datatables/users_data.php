<?php
	include '../../core/config.php';

	$data = mysqli_query($conn,"SELECT * FROM tbl_users WHERE role != 1");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$list = array();
		$list["count"] = $count++;
		$list["user_id"] = $row["user_id"];
		$list["name"] = $row["name"];
		$list["username"] = $row["username"];
		$list["branch"] = getBranchName($conn, $row["branch_id"]);
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>