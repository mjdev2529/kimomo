<?php
	session_start();
	include '../../core/config.php';
	$m_id = $_POST["m_id"];
	$c_id = $_POST["c_id"];

	function getScore($conn, $m_id, $uID, $ansType){
		$ans_data = mysqli_fetch_array(mysqli_query($conn, "SELECT *, count(*) as total_q FROM tbl_student_answers WHERE module_id = '$m_id' AND user_id = '$uID'"));

		if($ansType == 3 || $ansType == 4){
			$get_ans_sql = mysqli_query($conn, "SELECT sa.s_answer_id, count(*) as total_a FROM tbl_student_answers sa INNER JOIN tbl_module_answer ma ON ma.ma_id = sa.ma_id INNER JOIN tbl_module_question as mq ON mq.module_id = sa.module_id  WHERE sa.module_id = '$m_id' AND sa.user_id = '$uID' AND ma.is_correct = mq.mq_id");
		}else{
			$get_ans_sql = mysqli_query($conn, "SELECT sa.s_answer_id, count(*) as total_a FROM tbl_student_answers sa INNER JOIN tbl_module_answer ma ON ma.ma_id = sa.ma_id WHERE sa.module_id = '$m_id' AND sa.user_id = '$uID' AND ma.is_correct = 1");
		}
		$correct_ans = mysqli_fetch_array($get_ans_sql);
		$score = $correct_ans["total_a"]."/".$ans_data["total_q"];
		return $score;
	}

	function hasAnswered1($conn, $m_id, $uID){
		$ans_data = mysqli_fetch_array(mysqli_query($conn, "SELECT *, count(*) as total_q FROM tbl_student_answers WHERE module_id = '$m_id' AND user_id = '$uID'"));
		if($ans_data["total_q"] != 0){
			$has_answered = "<i class='fa fa-check-circle text-success'></i>";
		}else{
			$has_answered = "<i class='fa fa-times-circle text-dark'></i>";
		}

		return $has_answered;
	}

	$data = mysqli_query($conn,"SELECT *, cs.added_by as student FROM tbl_classes_student cs JOIN tbl_classes c ON cs.class_id = c.class_id AND cs.class_code = c.class_code WHERE c.added_by = '$_SESSION[uid]' AND c.class_id = '$c_id'");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$mData = mysqli_fetch_array(mysqli_query($conn,"SELECT content_type, answer_type FROM tbl_modules WHERE module_id = '$m_id'"));
		$actAnswer = mysqli_num_rows(mysqli_query($conn,"SELECT ma_id FROM tbl_module_answer WHERE mq_id = '$m_id' AND is_correct = '$row[student]'"));
		$viewActivity = "<button class='btn btn-outline-primary btn-sm' onclick='viewAct(".$m_id.",".$row['student'].")'>View</button>";

		$list = array();
		$list["count"] = $count++;
		$list["sclass_id"] = $row["sclass_id"];
		$list["student_name"] = strtoupper(getStudentName($conn, $row["student"]));
		$list["student_score"] = $mData[0] == 3?"":getScore($conn, $m_id, $row['student'], $mData[1]);
		$list["has_answered"] = $mData[0] == 3 && $actAnswer != 0?$viewActivity:hasAnswered1($conn, $m_id, $row['student']);
		// $list["date_added"] = $row["date_added"];
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>