<?php
	session_start();
	include '../../core/config.php';
	$s_id = $_POST["s_id"];
	$module_s = isset($_POST["pF"])?"AND content_type != 2":"";
	if($_SESSION["role"] == 1){
		$added = "AND added_by = '$_SESSION[uid]'".$module_s;
	}else{
		$added = "AND is_posted = 1";
	}

	$data = mysqli_query($conn,"SELECT * FROM tbl_modules WHERE subject_id = '$s_id' $added");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$ans_status = hasAnswered($conn, $row["module_id"], $_SESSION['uid']);
		$print_status = isPrintable($conn, $row["module_id"], $_SESSION['uid']);
		$status = date("Y-m-d", strtotime($row["module_deadline"])) < date("Y-m-d")?"Expired":($ans_status == 0?"Incomplete":"Completed");

		$list = array();
		$list["count"] = $count++;
		$list["module_id"] = $row["module_id"];
		$list["module_name"] = strtoupper($row["module_name"]);
		$list["module_deadline"] = $row["content_type"] == 1 || $row["content_type"] == 3?date("F d, Y", strtotime($row["module_deadline"])):"N/A";
		$list["date_added"] = $row["date_added"];
		$list["c_type"] = $row["content_type"] != 2 ?"Answer":"View";
		$list["content_type"] = $row["content_type"];
		$list["type"] = $row["content_type"] == 1?"Module":($row["content_type"] == 2?"Material":"Activity");
		$list["disable"] =  date("Y-m-d", strtotime($row["module_deadline"])) < date("Y-m-d")?2:($ans_status == 0?0:1);
		$list["status"] = $row["content_type"] == 2?"N/A":$status;
		$list["p_status"] = $print_status;


		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>