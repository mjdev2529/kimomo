<?php
  session_start();
  include '../core/config.php';

  $sql = mysqli_query($conn, "SELECT * FROM tbl_classes c INNER JOIN tbl_classes_student cs ON c.class_id = cs.class_id INNER JOIN tbl_announcement a ON c.added_by = a.user_id WHERE cs.added_by = '$_SESSION[uid]' AND a.announcement_id NOT IN (SELECT announcement_id FROM tbl_viewed WHERE user_id = '$_SESSION[uid]')");
  $countNotif = mysqli_num_rows($sql);
  $countViewedNotif = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM tbl_classes c INNER JOIN tbl_classes_student cs ON c.class_id = cs.class_id INNER JOIN tbl_announcement a ON c.added_by = a.user_id WHERE cs.added_by = '$_SESSION[uid]' AND a.announcement_id NOT IN (SELECT announcement_id FROM tbl_viewed WHERE user_id = '$_SESSION[uid]')"));
?>
<a class="nav-link text-dark" data-toggle="dropdown" href="#" aria-expanded="false">
  <i class="fa fa-bullhorn"></i>
  <?php if($countViewedNotif != 0){ ?>
    <span class="badge badge-warning navbar-badge"><?=$countNotif;?></span>
  <?php }?>
</a>
<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
  <span class="dropdown-item dropdown-header disabled">Announcements</span>
  <div class="dropdown-divider"></div>
  <?php if($countNotif != 0){while($row = mysqli_fetch_array($sql)){ ?>
  <a href="#" class="dropdown-item" onclick="viewNotif(<?=$row['announcement_id']?>)">
    <p>
      <i class="fas fa-bell mr-2"></i> Teacher <?=getTeacherName($conn, $row['user_id'])?> has an update
      <?php if($row["class_id"] != 0){ ?>
        for Class: <b><?=getClassName($conn, $row['class_id'])?></b>
      <?php } ?>
    </p>
    <!-- <span class="float-right text-muted text-sm">3 mins</span> -->
  </a>
  <div class="dropdown-divider"></div>
  <?php } }else{?>
    <a href="#" class="dropdown-item text-center">
      <p><i class="fas fa-info-circle mr-2"></i> No Data Available</p>
    </a>
    <div class="dropdown-divider"></div>
  <?php }?>
</div>