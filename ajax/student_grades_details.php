<?php
  session_start();
  include '../core/config.php';

  $c_id = $_POST["c_id"];
  $s_id = $_POST["s_id"];
  $st_id = $_POST["st_id"];

  function getScore($conn, $m_id, $uID, $ansType){
    $ans_data = mysqli_fetch_array(mysqli_query($conn, "SELECT *, count(*) as total_q FROM tbl_student_answers WHERE module_id = '$m_id' AND user_id = '$uID'"));

    if($ansType == 3 || $ansType == 4){
      $get_ans_sql = mysqli_query($conn, "SELECT sa.s_answer_id, count(*) as total_a FROM tbl_student_answers sa INNER JOIN tbl_module_answer ma ON ma.ma_id = sa.ma_id INNER JOIN tbl_module_question as mq ON mq.module_id = sa.module_id  WHERE sa.module_id = '$m_id' AND sa.user_id = '$uID' AND ma.is_correct = mq.mq_id");
    }else{
      $get_ans_sql = mysqli_query($conn, "SELECT sa.s_answer_id, count(*) as total_a FROM tbl_student_answers sa INNER JOIN tbl_module_answer ma ON ma.ma_id = sa.ma_id WHERE sa.module_id = '$m_id' AND sa.user_id = '$uID' AND ma.is_correct = 1");
    }
    $correct_ans = mysqli_fetch_array($get_ans_sql);
    $score = $correct_ans["total_a"] == 0 && $ans_data["total_q"] == 0?0:number_format(($correct_ans["total_a"]/$ans_data["total_q"])*100,2);
    $getGrade = mysqli_fetch_array(mysqli_query($conn,"SELECT grade FROM tbl_activity_grade WHERE module_id = '$m_id' AND added_by = '$_SESSION[uid]' AND student_id = '$uID'"));
    return $ansType == 0?(isset($getGrade[0])?$getGrade[0]:0):$score;
  }

  $cName = mysqli_fetch_array(mysqli_query($conn, "SELECT class_name FROM tbl_classes WHERE class_id = '$c_id'"));
  $sName = mysqli_fetch_array(mysqli_query($conn, "SELECT subject_name FROM tbl_subject WHERE subject_id = '$s_id'"));
?>
<style type="text/css">
  @media print {
    .btn {
      display: none;
    }
  }
</style>
<div class="row">
  <div class="mb-3 col-3 offset-9">
    <button class="btn btn-sm btn-block btn-outline-success" onclick="printDiv()">Print</button>
  </div>
  <div class="col-12 text-center mb-3">
    <h5>Grades Summary</h5>
  </div>
  <div class="col-12">
    <label>Student Name:</label> <?=strtoupper(getStudentName($conn, $st_id))?>
  </div>
  <div class="col-12">
    <label>Class:</label> <?=strtoupper($cName[0])?>
  </div>
  <div class="col-12 mb-3">
    <label>Subject:</label> <?=strtoupper($sName[0])?>
  </div>
  <div class="table-responsive col-12">
    <table id="tbl_students" class="table table-condensed">
        <thead>
        <tr>
            <th>Module Name</th>
            <th width="20" class="text-center">Type</th>
            <th width="50" class="text-center">Score</th>
        </tr>
        </thead>
        <tbody>
          <?php
            $mdlSql = mysqli_query($conn, "SELECT * FROM tbl_modules WHERE subject_id = '$s_id' AND (content_type = 1 OR content_type = 3)");
            $mRowCount = mysqli_num_rows($mdlSql);
            if($mRowCount != 0){

            $getTotal = 0;
            while($row = mysqli_fetch_array($mdlSql)){
              $getGrade = mysqli_fetch_array(mysqli_query($conn,"SELECT grade FROM tbl_activity_grade WHERE module_id = '$row[module_id]' AND added_by = '$_SESSION[uid]' AND student_id = '$st_id'"));
              $studentScore = getScore($conn, $row['module_id'], $st_id, $row['answer_type']);
              $getActAns = mysqli_num_rows(mysqli_query($conn, "SELECT ma_id FROM tbl_module_answer WHERE mq_id = '$row[module_id]' AND is_correct = '$st_id'"));
              $disabled = $getActAns == 0 || $row["module_deadline"] < date("Y-m-d")?"disabled":"";
          ?>
            <tr>
              <td><?=$row['module_name']?></td>
              <td><?=$row['content_type']==1?"Module":"Activity"?></td>
              <?php if($row['content_type']==1){?>
                <td class="text-center"><?=round($studentScore)?></td>
              <?php }else{ ?>
                <td class="text-center"><input type="number" id="act_grade" style="width: 50px;" onchange="addGrade(<?=$row['module_id']?>,<?=$st_id?>)" value="<?=$getGrade[0]?>" <?=$disabled?>></td>
              <?php } ?>
            </tr>
          <?php 
              $getTotal += getScore($conn, $row['module_id'], $st_id, $row['answer_type']);
           }}else{ ?>
            <tr>
              <td colspan="3" class="text-center">No data available</td>
            </tr>
          <?php } ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"><b>Final Grade:</b></td>
            <td class="text-center"><?=round($getTotal/$mRowCount)?></td>
          </tr>
        </tfoot>
    </table>
    </div>
</div>
<script type="text/javascript">
  function printDiv() 
  {

    var mywindow = window.open('', 'PRINT');

    mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    mywindow.document.write('<link rel="stylesheet" href="../assets/dist/css/adminlte.min.css"></head><body>');
    mywindow.document.write(document.getElementById("mdl-details").innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    setTimeout( function(){
      mywindow.print();
      mywindow.close();
    },200);

    return true;

  }

  function addGrade(mID,stID){
    var grade = $("#act_grade").val();
    var url = "../ajax/student_grades_add.php";
    $.ajax({
      url: url,
      type: "POST",
      data: {m_id: mID, actGrade: grade, st_id: stID},
      success: function(data){
        if(data == 1){
          alert("Success: grade was updated.");
          $("#module_details_md").modal("hide");
        }else{
          alert("Error: Something was wrong.");
        }
      }
    });
  }
</script>