<?php
  include '../core/config.php';
  session_start();
  if($_SESSION["in"] != 1){
    echo "Warning: Please login first!";
    echo "<script>alert('Warning: Please login first!'); window.location.href='../'</script>";
  }
  $page = isset($_GET['page'])&&$_GET['page']!=""?$_GET['page']:"404";
  $hide = $_SESSION['role'] == 1?"style='display:none;'":"";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KiMoMon</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../assets/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../assets/dist/css/adminlte.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    <!-- jQuery -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../assets/dist/js/adminlte.min.js"></script>
    <!-- ChartJS -->
    <script src="../assets/plugins/chart.js/Chart.min.js"></script>
    <!-- DataTables -->
    <script src="../assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="../assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- AdminLTE for demo purposes -->
    <!-- <script src="../assets/dist/js/demo.js"></script> -->

  </head>
  <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item dropdown notif" <?=$hide?>>
            <a class="nav-link text-dark" data-toggle="dropdown" href="#" aria-expanded="false">
              <i class="fa fa-bullhorn"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
              <span class="dropdown-item dropdown-header disabled">Announcements</span>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item text-center">
                <p><i class="fas fa-info-circle mr-2"></i> No Data Available</p>
              </a>
              <div class="dropdown-divider"></div>
          </li>

          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
              <i class="fas fa-bars"></i>
            </a>
            <div class="dropdown-menu dropdown-menu dropdown-menu-right">
              <a href="index.php?page=<?=page_url('profile')?>" class="dropdown-item">
                <i class="fas fa-user mr-2"></i> Profile
              </a>
              <?php if($_SESSION['role'] == 1){ ?>
                <div class="dropdown-divider"></div>
                <a href="index.php?page=<?=page_url('announcement')?>" class="dropdown-item">
                  <i class="fas fa-bullhorn mr-2"></i> Announcements
                </a>
              <?php } ?>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item" onclick="logout()">
                <i class="fas fa-sign-out-alt mr-2"></i> Sign out
              </a>
            </div>
          </li>

        </ul>
      </nav>

      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="index.php?page=<?=page_url('dashboard')?>" class="brand-link text-center">
          <!-- <img src="../assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
          <span class="brand-text font-weight-bolder">KiMoMon</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
          <!-- Sidebar user (optional) -->
          <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="col-12 text-center">
              <?php if($_SESSION['role'] == 1){?>
                <a href="#">Administrator</a>
              <?php }else{ ?>
                <a href="#"></a>
              <?php } ?>
            </div>
          </div> -->

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <?php include 'components/sidenav.php'; ?>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <?php include '../core/routes.php'; ?>
      </div>
      <!-- /.content-wrapper -->

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <div class="modal fade" id="view_notif_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-info-circle mr-1"></i> Announcement</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body notif-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $(document).ready( function(){
        setInterval( function(){
          getNotif();
        },10000);
      });

      //GLOBAL SCRIPT
      function logout(){
        var x = confirm("Are you sure to end your session?");
        if(x){
          window.location.href="../ajax/logout.php";
        }
      }

      function getNotif(){
        $.ajax({
          type: "POST",
          url: "../ajax/get_notif.php",
          success: function(data){
            $(".notif").html(data);
          }
        });
      }

      function viewNotif(aID){
        $.ajax({
          type: "POST",
          url: "../ajax/view_notif.php",
          data: {aID: aID},
          success: function(data){
            $("#view_notif_md").modal();
            $(".notif-body").html(data);
          }
        });
      }
    </script>
  </body>
</html>
