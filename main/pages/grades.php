<?php
  if(isset($_GET["qID"])){
    $qID = $_GET["qID"];
  }else{
    $qID = 0;
  }
  $q1 = $qID == 1?"selected":"";
  $q2 = $qID == 2?"selected":"";
  $q3 = $qID == 3?"selected":"";
  $q4 = $qID == 4?"selected":"";
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1>Grades</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title">Student List</h5>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-3 offset-9 mb-2">
                        <select id="qtr_id" class="form-control" onchange="get_subjects()">
                          <option value="0">Select Quarter:</option>
                          <option value="1" <?=$q1?>>Quarter 1</option>
                          <option value="2" <?=$q2?>>Quarter 2</option>
                          <option value="3" <?=$q3?>>Quarter 3</option>
                          <option value="4" <?=$q4?>>Quarter 4</option>
                        </select>
                      </div>
                      <div class="col-3 offset-9">
                        <select id="class_id" class="form-control" onchange="get_student_scores()">
                          <option value="0-0">Select Subject:</option>
                          <?php
                            if(isset($qID)){
                            $classSql = mysqli_query($conn, "SELECT * FROM tbl_classes c INNER JOIN tbl_subject s ON c.class_id = s.class_id WHERE c.added_by = '$_SESSION[uid]' AND s.quarter = '$qID' GROUP BY s.subject_id");
                            while($row = mysqli_fetch_array($classSql)){
                          ?>
                            <option value="<?=$row['class_id']?>-<?=$row['subject_id']?>"><?=$row['class_name']?> (<?=strtoupper($row['subject_name'])?>)</option>
                          <?php } }?>
                        </select>
                      </div>
                    </div>
                    <div class="col-10 offset-1 mt-3">
                      <table id="tbl_students" class="table table-condensed">
                          <thead>
                          <tr>
                              <th style="width: 10px"></th>
                              <th>Student Name</th>
                              <th width="100px"></th>
                          </tr>
                          </thead>
                          <tbody>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!-- /.card -->
              </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <!-- Add Modal -->
    <div class="modal fade" id="module_details_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body mdl-details" id="mdl-details">
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $(document).ready( function(){
            get_student_scores();
        });

        function get_student_scores(){
          var cID = $("#class_id").val();
          $("#tbl_students").DataTable().destroy();
          $("#tbl_students").dataTable({
          "ajax": {
              "type": "POST",
              "url": "../ajax/datatables/get_students_data.php",
              "data": {c_id: cID}
          },
          "processing": true,
          "bSort": false,
          "paging": false,
          "info": false,
          "columns": [
          {
              "mRender": function(data, type, row){
                return "<i class='fa fa-user-circle ml-1'></i>";
              }
          },
          {
              "data": "student_name"
          },
          {
              "mRender": function(data, type, row){
                return "<button class='btn btn-sm btn-outline-dark' onclick='moduleDetails("+row.class_id+","+row.subject_id+","+row.student_id+")'>View Details</button>";
              }
          }
          ]
          });
        }

        function moduleDetails(cID, sID, stID){
          var url = "../ajax/student_grades_details.php"
          $.ajax({
            url: url,
            type: "POST",
            data: {c_id: cID, s_id: sID, st_id: stID},
            success: function(data){
              $(".mdl-details").html(data);
              $("#module_details_md").modal();
            }
          });
        }


          function get_subjects(){
            var qtr_id = $("#qtr_id").val();
            if(qtr_id == 0){
              window.location.href="index.php?page=<?=page_url('grades')?>";
            }else{
              window.location.href="index.php?page=<?=page_url('grades')?>&qID="+qtr_id;
            }
          }
    </script>