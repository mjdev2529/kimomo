<?php
  if(isset($_GET["qID"])){
    $qID = $_GET["qID"];
  }else{
    $qID = 0;
  }

  $q1 = $qID == 1?"selected":"";
  $q2 = $qID == 2?"selected":"";
  $q3 = $qID == 3?"selected":"";
  $q4 = $qID == 4?"selected":"";
  $q5 = $qID == 5?"selected":"";

  if(isset($_GET["csID"]) && isset($_GET["sG"])){
    $csID = $_GET["csID"];
    $sG = $_GET["sG"];

    $gender = $sG == 1?"MALE":($sG == 3?"&nbsp;":"FEMALE");
    $gM = $sG == 1?"selected":"";
    $gF = $sG == 2?"selected":"";
    $gA = $sG == 3?"selected":"";
    
    if($csID != "-1"){
      $csIDData = explode("-", $csID);
      $c_id = $csIDData[0];
      $s_id = $csIDData[1];
      $cSql = "class_id = '$c_id'";
    }else{
      $cSql = "added_by = '$_SESSION[uid]'";
      $c_id = 0;
      $s_id = 0;
    }
    $c_data = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_classes WHERE $cSql"));
    $c_id = $qID == 5?$c_data["class_id"]:$csIDData[0];
    $getSY =  date("Y", strtotime($c_data["date_added"]))." - ".date("Y", strtotime("+ 1 year", strtotime($c_data["date_added"])));

  }else{
    $gM = "";
    $gF = "";
    $gA = "";
  }

  function getGrade($conn, $s_id, $uID){
    $mdlSql = mysqli_query($conn, "SELECT * FROM tbl_modules WHERE subject_id = '$s_id' AND (content_type = 1 OR content_type = 3)");
    $mRowCount = mysqli_num_rows($mdlSql);
    if($mRowCount != 0){

      $getTotal = 0;
      while($row = mysqli_fetch_array($mdlSql)){
        $m_id = $row["module_id"];
        $ans_data = mysqli_fetch_array(mysqli_query($conn, "SELECT *, count(*) as total_q FROM tbl_student_answers WHERE module_id = '$m_id' AND user_id = '$uID'"));

        if($row["answer_type"] == 3 || $row["answer_type"] == 4){
          $get_ans_sql = mysqli_query($conn, "SELECT sa.s_answer_id, count(*) as total_a FROM tbl_student_answers sa INNER JOIN tbl_module_answer ma ON ma.ma_id = sa.ma_id INNER JOIN tbl_module_question as mq ON mq.module_id = sa.module_id  WHERE sa.module_id = '$m_id' AND sa.user_id = '$uID' AND ma.is_correct = mq.mq_id");
        }else{
          $get_ans_sql = mysqli_query($conn, "SELECT sa.s_answer_id, count(*) as total_a FROM tbl_student_answers sa INNER JOIN tbl_module_answer ma ON ma.ma_id = sa.ma_id WHERE sa.module_id = '$m_id' AND sa.user_id = '$uID' AND ma.is_correct = 1");
        }
        $correct_ans = mysqli_fetch_array($get_ans_sql);
        $score = $correct_ans["total_a"] == 0 && $ans_data["total_q"] == 0?0:number_format(($correct_ans["total_a"]/$ans_data["total_q"])*100,2);
        $getGrade = mysqli_fetch_array(mysqli_query($conn,"SELECT grade FROM tbl_activity_grade WHERE module_id = '$m_id' AND added_by = '$_SESSION[uid]' AND student_id = '$uID'"));
        $getTotal += $row["answer_type"] == 0?(isset($getGrade[0])?$getGrade[0]:0):$score;
      }
    }else{
      $getTotal = 0;
    }

    return $getTotal == 0?0:round($getTotal/$mRowCount);
  }

  function getGradePerQuarter($conn, $s_id, $uID, $qtr){
    if($qtr == 1){
      $mdlSql = mysqli_query($conn, "SELECT * FROM tbl_modules WHERE subject_id = '$s_id' AND (content_type = 1 OR content_type = 3)");
    }else{
      $mdlSql = mysqli_query($conn, "SELECT * FROM tbl_modules m INNER JOIN tbl_subject s ON m.subject_id = s.subject_id WHERE s.header_id = '$s_id' AND s.quarter = '$qtr' AND (content_type = 1 OR content_type = 3)");
    }

    $mRowCount = $mdlSql?mysqli_num_rows($mdlSql):0;
    if($mRowCount != 0){

      $getTotal = 0;
      while($row = mysqli_fetch_array($mdlSql)){
        $m_id = $row["module_id"];
        $ans_data = mysqli_fetch_array(mysqli_query($conn, "SELECT *, count(*) as total_q FROM tbl_student_answers WHERE module_id = '$m_id' AND user_id = '$uID'"));

        if($row["answer_type"] == 3 || $row["answer_type"] == 4){
          $get_ans_sql = mysqli_query($conn, "SELECT sa.s_answer_id, count(*) as total_a FROM tbl_student_answers sa INNER JOIN tbl_module_answer ma ON ma.ma_id = sa.ma_id INNER JOIN tbl_module_question as mq ON mq.module_id = sa.module_id  WHERE sa.module_id = '$m_id' AND sa.user_id = '$uID' AND ma.is_correct = mq.mq_id");
        }else{
          $get_ans_sql = mysqli_query($conn, "SELECT sa.s_answer_id, count(*) as total_a FROM tbl_student_answers sa INNER JOIN tbl_module_answer ma ON ma.ma_id = sa.ma_id WHERE sa.module_id = '$m_id' AND sa.user_id = '$uID' AND ma.is_correct = 1");
        }
        $correct_ans = mysqli_fetch_array($get_ans_sql);
        $score = $correct_ans["total_a"] == 0 && $ans_data["total_q"] == 0?0:number_format(($correct_ans["total_a"]/$ans_data["total_q"])*100,2);
        $getGrade = mysqli_fetch_array(mysqli_query($conn,"SELECT grade FROM tbl_activity_grade WHERE module_id = '$m_id' AND added_by = '$_SESSION[uid]' AND student_id = '$uID'"));
        $getTotal += $row["answer_type"] == 0?(isset($getGrade[0])?$getGrade[0]:0):$score;
      }
    }else{
      $getTotal = 0;
    }

    return $mRowCount == 0?0:round($getTotal/$mRowCount);
  }

?>
<style type="text/css">
  @media print {
    .btn {
      display: none;
    }
  }
</style>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1>Class Record</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title">Student Module Summary List</h5>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="col-3 offset-9 mb-2">
                      <select id="qtr_id" class="form-control" onchange="get_subjects()">
                        <option value="0">Select Quarter:</option>
                        <option value="5" <?=$q5?>>All</option>
                        <option value="1" <?=$q1?>>Quarter 1</option>
                        <option value="2" <?=$q2?>>Quarter 2</option>
                        <option value="3" <?=$q3?>>Quarter 3</option>
                        <option value="4" <?=$q4?>>Quarter 4</option>
                      </select>
                    </div>

                    <?php if(isset($qID) && $qID != 5){ ?>
                      <div class="col-3 offset-9 mb-2">
                        <select id="class_id" class="form-control" onchange="get_gender()">
                          <option value="0-0">Select Subject:</option>
                          <?php
                            $classSql = mysqli_query($conn, "SELECT * FROM tbl_classes c INNER JOIN tbl_subject s ON c.class_id = s.class_id WHERE c.added_by = '$_SESSION[uid]' AND s.quarter = '$qID' GROUP BY s.subject_id");
                            while($row = mysqli_fetch_array($classSql)){
                              $selected = $c_id == $row["class_id"] && $s_id == $row["subject_id"]?"selected":"";
                          ?>
                            <option value="<?=$row['class_id']?>-<?=$row['subject_id']?>" <?=$selected?>><?=$row['class_name']?> (<?=strtoupper($row['subject_name'])?>)</option>
                          <?php } ?>
                        </select>
                      </div>
                    <?php }else{ ?>
                        <input type="hidden" id="class_id" value="-1">
                    <?php } ?>

                    <div class="col-3 offset-9">
                      <select id="student_gender" class="form-control" onchange="view_report()">
                        <option value="0">Select Gender:</option>
                        <option value="3" <?=$gA?>>ALL</option>
                        <option value="1" <?=$gM?>>MALE</option>
                        <option value="2" <?=$gF?>>FEMALE</option>
                      </select>
                    </div>
                    <hr>

                    <div class="row">
                      <?php 
                        if(isset($csID) && isset($sG) && $sG != '0'){
                      ?>

                        <div class="col-2 offset-10 mt-3">
                          <button class="btn btn-block btn-outline-dark" onclick="printDiv()">Print</button>
                        </div>
                        <div id="CR-data" class="col-10 offset-1 mt-3">
                          <h1 class="text-center mb-5">Summary of Quarterly Grades</h1>
                          <div class="col-4"><label class="mr-3">SCHOOL NAME:</label> <?=getSchoolName($conn,$_SESSION["uid"])?></div>
                          <table id="tbl_class_record" class="table table-condensed table-bordered">
                              <thead>
                                <tr>
                                  <th rowspan="2" class="text-center align-middle">LEARNER'S NAMES</th>
                                  <td><label class="mr-3">Grade & Section</label> <?=getClassName($conn, $c_id)?></td>
                                  <td colspan="5"><label class="mr-3">School Year</label> <?=$getSY?></td>
                                </tr>
                                <tr>
                                    <td><label class="mr-3">Teacher</label> <?=getTeacherName($conn,$_SESSION["uid"])?></td>
                                    <td colspan="5"><?php if(isset($s_id) && $s_id != 0){ ?><label class="mr-3">Subject</label> <?=strtoupper(getSubjectName($conn,$s_id))?><?php } ?></td>
                                </tr>
                                <tr>
                                  <th colspan="2"><?=$gender?></th>
                                  <th class="text-center" colspan="5"><?php if(isset($s_id)){ ?>STUDENT GRADE<?php } ?></th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                  if($qID != 5){
                                    $sGSql = $sG != 3?"AND u.sex = '$sG'":"";
                                  $data = mysqli_query($conn,"SELECT *, cs.added_by as student FROM tbl_classes_student cs JOIN tbl_classes c ON cs.class_id = c.class_id AND cs.class_code = c.class_code INNER JOIN tbl_subject s ON s.class_id = c.class_id INNER JOIN tbl_users u ON cs.added_by  = u.user_id  WHERE c.added_by = '$_SESSION[uid]' AND c.class_id = '$c_id' AND s.subject_id = '$s_id' $sGSql ORDER BY u.name ASC");
                                  while($row = mysqli_fetch_array($data)){
                                ?>
                                  <tr>
                                    <td colspan="2"><?=strtoupper($row["name"])?></td>
                                    <td class="text-center"><?=getGrade($conn, $s_id, $row["user_id"])?></td>
                                  </tr>
                                <?php 
                                    } 
                                  }else{ 
                                    $sGSql = $sG != 3?"AND u.sex = '$sG'":"";
                                    $data = mysqli_query($conn,"SELECT *, cs.added_by as student FROM tbl_classes_student cs JOIN tbl_classes c ON cs.class_id = c.class_id AND cs.class_code = c.class_code INNER JOIN tbl_subject s ON s.class_id = c.class_id INNER JOIN tbl_users u ON cs.added_by  = u.user_id  WHERE c.added_by = '$_SESSION[uid]' AND c.class_id = '$c_id' $sGSql GROUP BY u.user_id ORDER BY u.name ASC");
                                    while($row = mysqli_fetch_array($data)){
                                ?>
                                  <tr>
                                    <td colspan="2"><?=strtoupper($row["name"])?></td>
                                    <td class="text-center" colspan="5"><label>QUARTER</label></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2"><label>SUBJECT</label></td>
                                    <td>1st</td>
                                    <td>2nd</td>
                                    <td>3rd</td>
                                    <td>4th</td>
                                    <td width="105px">Final Rating</td>
                                  </tr>
                                  <?php
                                    $classSql = mysqli_query($conn, "SELECT * FROM tbl_classes c INNER JOIN tbl_subject s ON c.class_id = s.class_id WHERE c.added_by = '$_SESSION[uid]' AND s.header_id = 0 GROUP BY s.subject_id");
                                    while($sRow = mysqli_fetch_array($classSql)){
                                      $q1 = getGradePerQuarter($conn, $sRow["subject_id"], $row["user_id"], 1);
                                      $q2 = getGradePerQuarter($conn, $sRow["subject_id"], $row["user_id"], 2);
                                      $q3 = getGradePerQuarter($conn, $sRow["subject_id"], $row["user_id"], 3);
                                      $q4 = getGradePerQuarter($conn, $sRow["subject_id"], $row["user_id"], 4);

                                      $q1Grade = $q1 == 0?0:$q1;
                                      $q2Grade = $q2 == 0?0:$q2;
                                      $q3Grade = $q3 == 0?0:$q3;
                                      $q4Grade = $q4 == 0?0:$q4;

                                      $divisor = ($q1 == 0?0:1) + ($q2 == 0?0:1) + ($q3 == 0?0:1) + ($q4 == 0?0:1);
                                      $totalPerQuarter = $q1Grade + $q2Grade + $q3Grade + $q4Grade;

                                      $finalGrade = $totalPerQuarter==0?0:$totalPerQuarter/$divisor;
                                  ?>
                                    <tr>
                                      <td colspan="2" class="pl-5"><label><?=strtoupper($sRow["subject_name"])?></label></td>
                                      <td><?=getGradePerQuarter($conn, $sRow["subject_id"], $row["user_id"], 1)?></td>
                                      <td><?=getGradePerQuarter($conn, $sRow["subject_id"], $row["user_id"], 2)?></td>
                                      <td><?=getGradePerQuarter($conn, $sRow["subject_id"], $row["user_id"], 3)?></td>
                                      <td><?=getGradePerQuarter($conn, $sRow["subject_id"], $row["user_id"], 4)?></td>
                                      <td><?=number_format($finalGrade,1)?></td>
                                    </tr>
                                  <?php } ?>
                                <?php }} ?>
                              </tbody>
                          </table>
                        </div>
                      <?php }else{ ?>
                        <div class="col-10 offset-1 mt-3 text-center">
                          <h3>No data available.</h3>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <!-- /.card -->
              </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>


    <script type="text/javascript">
      $(document).ready( function(){
            // get_student_scores();
        });

      function printDiv() 
      {

        var mywindow = window.open('', 'PRINT');

        mywindow.document.write('<html><head><title>' + document.title  + '</title>');
        mywindow.document.write('<link rel="stylesheet" href="../assets/dist/css/adminlte.min.css"></head><body>');
        mywindow.document.write(document.getElementById("CR-data").innerHTML);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        setTimeout( function(){
          mywindow.print();
          mywindow.close();
        },200);

        return true;

      }

      function get_subjects(){
        var qtr_id = $("#qtr_id").val();
        if(qtr_id == 0){
          window.location.href="index.php?page=<?=page_url('class_record')?>";
        }else{
          window.location.href="index.php?page=<?=page_url('class_record')?>&qID="+qtr_id;
        }
      }

      function get_gender(){
          $("#student_gender").val("0");
      }

      function view_report(){
        var csID = $("#class_id").val();
        var sG = $("#student_gender").val();
        var qID = "<?=$qID?>";
        if(csID != "0-0"){
          window.location.href="index.php?page=<?=page_url('class_record')?>&csID="+csID+"&sG="+sG+"&qID="+qID;
        }else{
          alert("Warning: No class selected.");
          $("#student_gender").val("0");
        }
      }
    </script>