<?php
  $hide_s = $_SESSION['role'] == 0?"style='display:none;'":"";
  $hide = $_SESSION['role'] == 1?"style='display:none;'":"";
?>
<style>
  .cards tbody tr {
      float: left;
      width: 20rem;
      margin: 0.5rem;
      border: 0.0625rem solid rgba(0,0,0,.125);
      border-radius: .25rem;
      box-shadow: 0.25rem 0.25rem 0.5rem rgba(0,0,0,0.25);
  }
  .cards tbody td {
      display: block;
  }
  .table tbody label {
      display: none;
  }
  .cards tbody label {
    display: inline;
    position: relative;
    font-size: 85%;
    top: -0.5rem;
    float: left;
    color: #000;
    min-width: 4rem;
    margin-left: 0;
    margin-right: 1rem;
    text-align: left;
  }
  tr.selected label {
      color: #404040;
  }
  
  .table .fa {
      font-size: 2.5rem;
      text-align: center;
  }
  .cards .fa {
      font-size: 7.5rem;
  }
  
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1>Lessons</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title">Lessons List</h3>
                  <div class="card-tools" <?=$hide_s?>>
                      <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_lesson_md">
                        Add
                      </button>
                      <!-- <button type="button" class="btn btn-sm btn-danger" onclick="delete_class()">
                        Delete
                      </button> -->
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <?php
                      if($_SESSION["role"] == 0){
                    ?>
                      <h1>Class Learning Materials</h1>
                      <div class="row card-deck">
                        <?php
                            $getLessons = mysqli_query($conn,"SELECT * FROM `tbl_classes` c INNER JOIN tbl_classes_student cs ON c.class_id = cs.class_id INNER JOIN tbl_subject s ON c.class_id = s.class_id INNER JOIN `tbl_modules` m ON s.subject_id = m.subject_id WHERE cs.added_by = '$_SESSION[uid]' AND m.content_type = 2");
                            while($row = mysqli_fetch_array($getLessons)){
                        ?>
                            <div class="col-12 col-md-3 card pt-3 pb-2">
                              <div class="row">
                                <h1 class="text-center col-12"><i class="fa fa-book"></i></h1>
                                <div class="col-12">
                                  <hr>
                                  <label class="text-left">Class:</label>
                                  <div class="text-center">
                                    <?=strtoupper(getClassName($conn, $row["class_id"]))?>
                                  </div>
                                </div>
                                <div class="col-12">
                                  <hr>
                                  <label class="text-left">Subject:</label>
                                  <div class="text-center">
                                    <?=strtoupper(getSubjectName($conn, $row["subject_id"]))?>
                                  </div>
                                </div>
                                <div class="col-12">
                                  <hr>
                                  <label class="text-left">Name:</label>
                                  <div class="text-center"><?=strtoupper($row["module_name"])?></div>
                                  <hr>
                                </div>
                                <a href="index.php?page=<?=page_url('module_answer')?>&s_id=<?=$row['subject_id']?>&m_id=<?=$row['module_id']?>&c_id=<?=$row['class_id']?>&pF=classes" class="btn btn-primary col-10 offset-1" target="_blank">View</a>
                              </div>
                            </div>
                        <?php } ?>
                      </div>
                    <?php }
                    // else{ ?>
                      
                      <h1 class="mt-3">Lessons</h1>
                      <div class="row m-3">
                        <div class="col">
                          <table id="tbl_lesson" class="table" cellspacing="0">
                            <thead>
                              <tr>
                                <th></th>
                                <th></th>
                                <th>Class</th>
                                <th>Subject</th>
                                <th>Lesson Name</th>
                                <th>Lesson Data</th>
                              </tr>
                            </thead>
                            <tbody>

                            </tbody>
                          </table>
                        </div>
                      </div>
                    <?php //} ?>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <!-- Add Modal -->
    <div class="modal fade" id="add_lesson_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add lesson</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="add_lesson_form" method="POST" action="#" enctype="multipart/form-data">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Class</label>
                  <select name="class_id" class="form-control" onchange="subjectList()">
                    <option value="">Select:</option>
                    <?php
                      $getClass = mysqli_query($conn,"SELECT class_id, class_name FROM tbl_classes WHERE added_by = '$_SESSION[uid]'");
                      while($cRow = mysqli_fetch_array($getClass)){
                    ?>
                      <option value="<?=$cRow[0]?>"><?=$cRow[1]?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Subject</label>
                  <select name="subject_id" class="form-control">
                    <option value="">Select:</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="lesson_name" class="form-control" placeholder="Name">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Type</label>
                  <select name="l_type" class="form-control" onchange="lessonType()">
                    <option value="">Select:</option>
                    <option value="1">Link</option>
                    <option value="2">File</option>
                  </select>
                </div>
                <div class="form-group link" style="display: none;">
                  <label for="exampleInputEmail1">Link</label>
                  <input type="text" name="lesson_link" class="form-control" placeholder="Link">
                </div>
                <div class="form-group file" style="display: none;">
                  <label for="exampleInputEmail1">File</label><br>
                  <input type="file" name="lesson_file">
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $(document).ready( function(){
        get_lesson();
        $("#tbl_lesson").toggleClass("cards");
        $("#tbl_lesson thead").toggle();
    
        if($("#tbl_lesson").hasClass("cards")){
            var max = 0;
            $('#tbl_lesson tr').each(function() {
                max = Math.max($(this).height(), max);
            }).height(max);
        } else {
            $("#tbl_lesson tr").each(function(){
                $(this).height("auto");
            });
        }
      });

      function get_lesson(){
        // $("#tbl_lesson").DataTable().destroy();
        // $("#tbl_lesson").dataTable({
        //   "ajax": {
        //     "type": "POST",
        //     "url": "../ajax/datatables/lesson_data.php",
        //   },
        //   "processing": true,
        //   "bPaginate": false,
        //   "bLengthChange": false,
        //   "bFilter": true,
        //   "bInfo": false,
        //   "bSort": false,
        //   "columns": [
        //   {
        //     "mRender": function(data, type, row){
        //       return "<i class='fa fa-book'></i>";
        //     }
        //   },
        //   {
        //     "data": "lesson_name"
        //   },
        //   {
        //     "data": "lesson_data"
        //   }
        //   ]
        // });

        var table = $('#tbl_lesson').DataTable({
                dom: 'fBti',
                // pageLength: -1,
                // buttons: [
                //     {
                //         text: '<i class="fa fa-id-badge fa-fw fa-lg" aria-hidden="true"></i>',
                //         className: 'animated bounce',
                //         action: function () {
                            
                //             $("#tbl_lesson").toggleClass("cards");
                //             $("#tbl_lesson thead").toggle();
                //             $("#card-toggle .fa").toggleClass("fa-table");
                //             $("#card-toggle .fa").toggleClass("fa-id-badge");
                        
                //             if($("#tbl_lesson").hasClass("cards")){
                //                 var max = 0;
                //                 $('#tbl_lesson tr').each(function() {
                //                     max = Math.max($(this).height(), max);
                //                 }).height(max);
                //             } else {
                //                 $("#tbl_lesson tr").each(function(){
                //                     $(this).height("auto");
                //                 });
                //             }

                //         },
                //         attr:  {
                //             title: 'Change views',
                //             id: 'card-toggle'
                //         }
                //     }
                // ],
                select: 'single',
                "ajax": {
                  "type": "POST",
                  "url": "../ajax/datatables/lesson_data.php",
                },
                "processing": true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bSort": false,
                columns: [
                    {   /* created column to show a picture just to make this demo look better */
                        orderable: false, data: 'Photo', name: '', orderable: false, defaultContent: '', title: '',
                        visible: true, className: 'text-center', width: '20px',
        
                        createdCell: function (td, cellData, rowData, row, col) {
                            var $ctl = '<i class="fa fa-book fa-fw"></i>';
                            $(td).append($ctl);
                        }
                    },
                    /* I added a label to the column for the field name which will show up in the card display */
                    {
                        data: "class", name: "class",
                        render: function (data, type, row, meta) { return (type === 'display' ? '<label>Class:</label>' : null ) + data; }
                    },
                    {
                        data: "subject", name: "subject",
                        render: function (data, type, row, meta) { return (type === 'display' ? '<label>Subject:</label>' : null ) + data; }
                    },
                    {
                        data: "lesson_name", name: "lesson_name",
                        render: function (data, type, row, meta) { return (type === 'display' ? '<label>Lesson Name:</label>' : null ) + data; }
                    },
                    {
                        data: "lesson_data", name: "lesson_data",
                        render: function (data, type, row, meta) { return (type === 'display' ? '<label>'+ row.type +':</label>' : null ) + data; }
                    },
                    {
                        data: "delete", name: "delete",
                        render: function (data, type, row, meta) { return (type === 'display' ? "<button class='btn btn-outline-danger btn-block' onclick='delete_lesson("+row.lesson_id+")' <?=$hide_s?>>Delete</button>" : null ); }
                    }
                ]
            });
      }

      function lessonType(){
        var type = $("select[name=l_type]").val();
        if(type == 1){
          $(".link").show();
          $(".file").hide();
        }else{
          $(".link").hide();
          $(".file").show();
        }
      }

      $("#add_lesson_form").submit( function(e){
        e.preventDefault();
        var data = new FormData(this);
        var url = "../ajax/lesson_add.php";
         
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          contentType: false,
          processData: false,
          success: function(data){
            if(data == 1){
              alert("Success: New lesson was added");
              window.location.reload();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function delete_lesson(lessonID){
        var url = "../ajax/lesson_delete.php";
        var conf = confirm("Are you sure to delete this lesson?");
        $.ajax({
          type: "POST",
          url: url,
          data: {lesson_id: lessonID},
          success: function(data){
            if(data == 1){
              alert("Success: Selected lesson was removed");
              window.location.reload();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      }

      function subjectList(){
        var cID = $("select[name=class_id]").val();
        var url = "../ajax/subject_list.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {cID: cID},
          success: function(data){
            $("select[name=subject_id]").html(data);
          }
        });
      }
    </script>