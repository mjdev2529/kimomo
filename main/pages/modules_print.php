<?php
  $m_id = $_GET["m_id"];
  $c_id = $_GET["c_id"];
  $s_id = $_GET["s_id"];

  $mData = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_modules WHERE module_id = '$m_id'"));
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1>Module Print</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
              <div class="col-md-12">
                <div class="col-2 offset-10 mb-3">
                  <button class="btn btn-outline-dark btn-block" onclick="printData()">Print</button>
                </div>
                <div class="card">
                  <div class="card-body">
                    <div class="row" id="printable">
                      <div class="col-10 offset-1 h4">Name: <?=getStudentName($conn, $_SESSION["uid"])?></div>
                      <div class="col-10 offset-1 ">Class: <?=getClassName($conn, $c_id)?></div>
                      <div class="col-10 offset-1 ">Subject: <?=getSubjectName($conn, $s_id)?></div>
                      <div class="col-12 mt-3 mb-3" style="border: 1px solid; border-color: #ccc;"></div>
                      <div class="col-12 h5 text-center"><?=strtoupper($mData["module_name"])?></div>
                      <div class="col-10 offset-1"><label>Instructions:</label> <?=strtoupper($mData["module_instructions"])?></div>

                        <!-- DRAG N DROP -->
                      <?php if($mData["answer_type"] == 4){?>

                        <div class="col-10 offset-1 row">
                          <div class="col-6 mt-3">
                            <?php
                              $mq_sql = mysqli_query($conn,"SELECT * FROM tbl_module_question WHERE module_id = '$m_id'");
                              $mq_count = 1;
                              while($mqRow = mysqli_fetch_array($mq_sql)){
                            ?>
                              <div class="col-12 mb-3">
                                <label><?=$mq_count++?>.)</label>
                                <img src="<?=$mqRow["module_question"]?>" width="100" height="100">
                              </div>
                            <?php } ?>
                          </div>
                          <div class="col-6 mt-3">
                            <?php
                              $ma_sql = mysqli_query($conn,"SELECT * FROM tbl_module_answer WHERE mq_id = '$m_id'");
                              $ma_count = 0;
                              while($maRow = mysqli_fetch_array($ma_sql)){
                                $a = range('A', 'Z');
                            ?>
                              <div class="col-12 mb-3">
                                <label><?=strtoupper($a[$ma_count++])?>.)</label>
                                <img src="<?=$maRow["module_answer"]?>" width="100" height="100">
                              </div>
                            <?php } ?>
                          </div>
                        </div>

                      <?php }else{ ?>
                        <!-- NON DRAG N DROP -->

                        <div class="col-10 offset-1 mt-3">
                          <?php
                            $mq_sql = mysqli_query($conn,"SELECT * FROM tbl_module_question WHERE module_id = '$m_id'");
                            $mq_count = 1;
                            while($mqRow = mysqli_fetch_array($mq_sql)){
                          ?>
                            <div class="col-12"><label><?=$mq_count++?>.) <?=$mqRow["module_question"]?></label></div>
                            <div class="col-10 offset-1 row">
                              <?php
                                $ma_sql = mysqli_query($conn,"SELECT * FROM tbl_module_answer WHERE mq_id = '$mqRow[mq_id]'");
                                $ma_count = 0;
                                while($maRow = mysqli_fetch_array($ma_sql)){
                                  $a = range('A', 'Z');
                              ?>
                                <div class="col"><label><?=strtoupper($a[$ma_count++])?>.) <?=$maRow["module_answer"]?></label></div>
                              <?php } ?>
                            </div>
                          <?php } ?>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <!-- /.card -->
              </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <script type="text/javascript">
      $(document).ready( function(){
        printData();
      });

      function printData() 
      {

        var mywindow = window.open('', 'PRINT');

        mywindow.document.write('<html><head><title>' + document.title  + '</title>');
        mywindow.document.write('<link rel="stylesheet" href="../assets/dist/css/adminlte.min.css"></head><body>');
        mywindow.document.write(document.getElementById("printable").innerHTML);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        setTimeout( function(){
          mywindow.print();
          mywindow.close();
        },200);

        return true;

      }
      
    </script>