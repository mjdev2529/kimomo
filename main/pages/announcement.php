<?php
  $hide_s = $_SESSION['role'] == 0?"style='display:none;'":"";
  $c_id = "";
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1>Announcement</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
            <!-- <div class="col-6 mb-3">
              <h1>Class: <?=$row["class_name"]?></h1>
            </div> -->
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title">Announcement List</h5>
                  <div class="card-tools" <?=$hide_s?>>
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_subject_md">
                      Add
                    </button>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_announcement()">
                      Delete
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="tbl_subject" class="table table-condensed table-bordered">
                    <thead>
                      <tr>
                        <th style="width: 10px"><input type="checkbox" id="checkAllClass" onclick="checkAll()"></th>
                        <th style="width: 10px">#</th>
                        <th>Announcement</th>
                        <th style="width: 200px">Class</th>
                        <th style="width: 100px">Date Added</th>
                        <th style="width: 100px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <!-- Add Modal -->
    <div class="modal fade" id="add_subject_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Announcement</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="add_announcement_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Class Name</label>
                  <select class="form-control" name="class_id">
                    <option value="0">All Class</option>
                    <?php
                      $sql = mysqli_query($conn,"SELECT * FROM tbl_classes WHERE added_by = '$_SESSION[uid]'");
                      while($row = mysqli_fetch_array($sql)){
                    ?>
                      <option value="<?=$row['class_id']?>"><?=$row['class_name']?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Announcement</label>
                  <textarea name="announcement" class="form-control" placeholder="Type Here..."></textarea>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="edit_subject_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Announcement</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="edit_announcement_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Class Name</label>
                  <select class="form-control" name="class_id" id="class_id">
                    <option value="0">All Class</option>
                    <?php
                      $sql = mysqli_query($conn,"SELECT * FROM tbl_classes WHERE added_by = '$_SESSION[uid]'");
                      while($row = mysqli_fetch_array($sql)){
                    ?>
                      <option value="<?=$row['class_id']?>"><?=$row['class_name']?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Announcement</label>
                  <textarea name="announcement" id="announcement" class="form-control" placeholder="Type Here..."></textarea>
                  <input type="hidden" name="announcement_id" id="announcement_id" >
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $(document).ready( function(){
        get_announcement();
      });

      function get_announcement(){
        $("#tbl_subject").DataTable().destroy();
        $("#tbl_subject").dataTable({
          "ajax": {
            "type": "POST",
            "url": "../ajax/datatables/announcement_data.php"
          },
          "processing": true,
          "columns": [
          {
            "mRender": function(data, type, row){
              return "<input type='checkbox' value='"+row.announcement_id+"' name='cb_announcement'>";
            }
          },
          {
            "data": "count"
          },
          {
            "data": "announcement"
          },
          {
            "data": "class"
          },
          {
            "data": "date_added"
          },
          {
            "mRender": function(data, type, row){
              return "<button class='btn btn-sm btn-outline-dark btn-block' onclick='edit_announcement("+row.announcement_id+")' <?=$hide_s?>>Edit data</button>";
            }
          }
          ]
        });
      }

      function checkAll(){
        var x = $("#checkAllClass").is(":checked");
        if(x){
          $("input[name=cb_announcement]").prop("checked", true);
        }else{
          $("input[name=cb_announcement]").prop("checked", false);
        }
      }

      $("#add_announcement_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/announcement_add.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              alert("Success: New announcement was added.");
              $("#add_subject_md").modal("hide");
              $("textarea").val("");
              $("select").val("0");

              get_announcement();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function edit_announcement(aID){
        var url = "../ajax/announcement_details.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {aID: aID},
          success: function(data){
            $("#edit_subject_md").modal();
            var o = JSON.parse(data);
            $("#announcement_id").val(aID);
            $("#announcement").val(o.announcement);
            $("#class_id").val(o.class_id);
          }
        });
      }

      $("#edit_announcement_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/announcement_update.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              alert("Success: Announcement details was updated.");
              $("#edit_subject_md").modal("hide");
              $("textarea").val("");
              $("select").val("0");

              get_announcement();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function delete_announcement(){
        var conf = confirm("Are you sure to delete selected?");
        if(conf){
          var announcement = [];
          $("input[name=cb_announcement]:checked").each( function(){
            announcement.push($(this).val());
          });

          if(announcement.length != 0){
            var url = "../ajax/announcement_delete.php";
            $.ajax({
              type: "POST",
              url: url,
              data: {aID: announcement},
              success: function(data){
                if(data != 0){
                  alert("Success: Selected announcement/s was removed.");
                  get_announcement();
                }else{
                  alert("Error: Something is wrong.");
                }
              }
            });
          }else{
            alert("Warning: No data selected.");
          }
        }
      }

      // function subject_details(sID){
      //   var c_id = "<?=$c_id?>";
      //   window.location.href="index.php?page=<?=page_url('subject_details')?>&c_id="+c_id+"&s_id="+sID;
      // }
    </script>