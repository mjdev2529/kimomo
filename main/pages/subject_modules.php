<?php
  $s_id = $_GET["s_id"];
  $c_id = $_GET["c_id"];
  $pF = $_GET["pF"];
  $row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_subject WHERE subject_id = '$s_id'"));

  if(isset($_GET["m_id"])){
    $m_id = $_GET["m_id"];
    $row1 = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_modules WHERE module_id = '$m_id'"));

    $show_details = $row1["answer_type"] == 1 || $row1["answer_type"] == 2?"":"display:none;";
    $mname = isset($row1["module_name"])?"value='".$row1["module_name"]."' readonly":"";
    $deadline = isset($row1["module_deadline"])?"value='".$row1["module_deadline"]."' readonly":"";
    $ins_state = isset($row1["module_instructions"])?"readonly":"";
    $ins_data = isset($row1["module_instructions"])?$row1["module_instructions"]:"";

    $a_type = isset($row1["answer_type"]) && $row1["answer_type"] != 0?"disabled":"";
    $c_type = isset($row1["content_type"]) && $row1["content_type"] != 0?"disabled":"";
    $disabled = isset($row1["module_id"])?"disabled":"";

    $dd1 = $row1["answer_type"] == 1?"selected":"";
    $dd2 = $row1["answer_type"] == 2?"selected":"";
    $dd3 = $row1["answer_type"] == 3?"selected":"";
    $dd4 = $row1["answer_type"] == 4?"selected":"";

    $cDD1 = $row1["content_type"] == 1?"selected":"";
    $cDD2 = $row1["content_type"] == 2?"selected":"";
    $cDD3 = $row1["content_type"] == 3?"selected":"";

    if($row1["content_type"] == 2 || $row1["content_type"] == 3){
      $lm_desc = $row1["module_instructions"];
    }

    $show_module_dd = "";
  }else{
    $show_details = "display:none;";
    $mname = "";
    $deadline = "";
    $ins_state = "";
    $ins_data = "";
    $a_type = "";
    $c_type = "";
    $disabled = "";

    $dd1 = "";
    $dd2 = "";
    $dd3 = "";
    $dd4 = "";

    $cDD1 = "";
    $cDD2 = "";
    $cDD3 = "";

    $lm_desc = "";
    $show_module_dd = "display:none;";
  }
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1><a href="index.php?page=<?=page_url('subject_details')?>&s_id=<?=$s_id?>&c_id=<?=$c_id?>&pF=<?=$pF?>"><i class="fa fa-chevron-left"></i> Subject</a> / Module Details</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title">Content for subject: <?=strtoupper($row["subject_name"])?></h5>
                  <div class="card-tools">
                    <!-- <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_module_md">
                      Add
                    </button>
                    <button type="button" class="btn btn-sm btn-outline-success" onclick="post_module()">
                      Post Module
                    </button> -->
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body ">
                  <div class="row">
                    <div class="form-group col-3 offset-6 text-right pt-2">
                      <label for="exampleInputEmail1">Content Type</label>
                    </div>
                    <div class="form-group col-3">
                      <select class="form-control" id="content_type" <?=$c_type?> onchange="select_content()">
                        <!-- <option value="0">Select Type:</option> -->
                        <option value="1" <?=$cDD1?>>Module</option>
                        <option value="2" <?=$cDD2?>>Learning Material</option>
                        <option value="3" <?=$cDD3?>>Activity</option>
                      </select>
                    </div>

                    <div class="col-12 modules">
                      <form id="add_module_form" method="POST" action="#">
                        <div class="row">
                          <div class="form-group col-3 offset-1">
                            <label for="exampleInputEmail1">Module Name</label>
                            <input type="text" name="module_name" class="form-control" placeholder="Module Name" <?=$mname?> required>
                            <input type="hidden" name="subject_id" value="<?=$s_id?>">
                          </div>
                          <div class="form-group col-3">
                            <label for="exampleInputEmail1">Module Deadline</label>
                            <input type="date" name="module_deadline" class="form-control" placeholder="Module Deadline" <?=$deadline?> required>
                          </div>
                          <div class="form-group col-3">
                            <label for="exampleInputEmail1">Answer Type</label>
                            <select class="form-control" name="answer_type" <?=$a_type?>>
                              <option value="0">Select Type:</option>
                              <option value="1" <?=$dd1?>>Radio Button</option>
                              <option value="2" <?=$dd2?>>Dropdown</option>
                              <option value="3" <?=$dd3?>>Drag and drop</option>
                              <option value="4" <?=$dd4?>>Drag and drop - Graphic</option>
                            </select>
                          </div>
                          <div class="form-group col-9 offset-1">
                            <label for="exampleInputEmail1">Module Instructions</label>
                            <textarea class="form-control" name="module_instructions" placeholder="Type Here..." <?=$ins_state?> required><?=$ins_data?></textarea>  
                          </div>
                          <div class="form-group col-2 offset-10 pt-2">
                            <button type="submit" class="btn btn-primary btn-block" <?=$disabled?>>Add</button>
                          </div>
                        </div>
                      </form>

                      <hr>
                      <div class="row" style="<?=$show_details?>">
                        <div class="col-8 offset-2">
                          <form id="add_mq_form" method="POST" action="#">
                            <div class="row">
                              <div class="form-group col-12">
                                <label>Add question</label>
                                <textarea name="module_question" class="form-control" placeholder="Type Here..."></textarea>
                                <input type="hidden" name="module_id" value="<?=$m_id?>">
                              </div>
                              <div class="form-group col-12">
                                <button class="btn btn-primary col-2 offset-10">Add Question</button>
                              </div>
                            </div>
                          </form>

                        <!-- question -->
                        <div class="row">
                          <?php
                            $fetch_module_question = mysqli_query($conn, "SELECT * FROM tbl_module_question WHERE module_id = '$m_id'");
                            if($fetch_module_question){
                              $count = 1;
                              while ($mq_data = mysqli_fetch_array($fetch_module_question)) {
                          ?>
                            <div class="col-12 p-3 card ">
                              <div class="row module_detail">
                                <div class="form-group col-10">
                                  <label><?=$count++?>. <?=$mq_data["module_question"]?></label>
                                  <hr>
                                  <textarea class="form-control" placeholder="Type Here..." id="ma_field<?=$mq_data['mq_id']?>"></textarea>
                                </div>
                                <div class="form-group col-1 pl-0">
                                  <button class="btn btn-danger btn-sm" onclick="remove_question(<?=$mq_data['mq_id']?>)">Remove</button>
                                </div>
                                <div class="col-2 offset-8">
                                  <label><input type="checkbox" id="cb_ans<?=$mq_data['mq_id']?>" class="mr-1"> Correct answer</label>
                                </div>
                                <div class="form-group col-2">
                                  <button class="btn btn-outline-primary" onclick="add_answer(<?=$mq_data['mq_id']?>)">Add Answer</button>
                                </div>


                                <table id="tbl_answers" class="table table-sm table-condensed">
                                <thead>
                                  <tr>
                                    <th>Answer</th>
                                    <th width="100px">Correct Answer</th>
                                    <th width="100px"></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    $fetch_module_answer = mysqli_query($conn, "SELECT * FROM tbl_module_answer WHERE mq_id = '$mq_data[mq_id]'");
                                    $count_ans = mysqli_num_rows($fetch_module_answer);
                                    if(mysqli_num_rows($fetch_module_answer) != 0){
                                      while ($ma_data = mysqli_fetch_array($fetch_module_answer)) {
                                  ?>
                                    <tr>
                                      <td><?=$ma_data['module_answer']?></td>
                                      <td><?=$ma_data['is_correct']==1?"Yes":"No";?></td>
                                      <td>
                                        <button class="btn btn-outline-danger btn-sm" onclick="remove_answer(<?=$ma_data['ma_id']?>)">Remove</button>
                                      </td>
                                    </tr>
                                  <?php } }else{?>
                                    <tr>
                                      <td colspan="2" class="text-center">No data available</td>
                                    </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                              </div>
                            </div>
                          <?php } } ?>

                        </div>
                        <!-- question -->

                      </div>
                    </div>
                      <!-- DRAG N DROP -->
                      <?php if(isset($row1["answer_type"]) && $row1["answer_type"] == 3){ ?>
                        <div class="col-12 drag-drop" style="<?=$row1['answer_type']==3?$show_module_dd:'display: none;';?>">
                          <div class="row card-deck">
                            <div class="col-6 p-3 card ">
                          
                              <form id="add_mq_form2" method="POST" action="#">
                                <div class="row">
                                  <div class="form-group col-12">
                                    <label>Add question</label>
                                    <textarea name="module_question" class="form-control" placeholder="Type Here..."></textarea>
                                    <input type="hidden" name="module_id" value="<?=$m_id?>">
                                  </div>
                                  <div class="form-group col-12">
                                    <button class="btn btn-primary col-4 offset-8">Add Question</button>
                                  </div>
                                </div>
                              </form>
                              <hr>
                              <label>Column A</label>
                              <hr>
                              <?php
                                $fetch_module_question = mysqli_query($conn, "SELECT * FROM tbl_module_question WHERE module_id = '$m_id'");
                                if(mysqli_num_rows($fetch_module_question) != 0){
                                  $count = 1;
                                  while ($mq_data = mysqli_fetch_array($fetch_module_question)) {
                              ?>
                                <div class="row module_detail">
                                  <div class="form-group col-10">
                                    <label><?=$count++?>. <?=$mq_data["module_question"]?></label>
                                  </div>
                                  <div class="form-group col-1 pl-0">
                                    <button class="btn btn-danger btn-sm" onclick="remove_question(<?=$mq_data['mq_id']?>)">Remove</button>
                                  </div>
                                </div>
                              <?php } }else{?>
                                <tr>
                                  <td colspan="2" class="text-center">No data available</td>
                                </tr>
                              <?php } ?>
                              </div>
                              <div class="col-6 p-3 card ">
                                <label>Add answer</label>
                                <div class="row module_detail">
                                  <div class="form-group col-12">
                                    <textarea class="form-control" placeholder="Type Here..." id="ma_field2"></textarea>
                                  </div>
                                  <label class="col-4 pl-0 text-right">Set as answer for:</label>
                                  <div class="form-group col-4 pl-0">
                                    <select name="mq_answer_correct" class="form-control">
                                      <option value="">Select:</option>
                                      <?php
                                        $mq_sql = mysqli_query($conn, "SELECT * FROM tbl_module_question WHERE mq_id NOT IN (SELECT is_correct FROM tbl_module_answer) AND module_id = '$m_id'") or die(mysqli_error($conn));
                                        $count = 0;
                                        while($row = mysqli_fetch_array($mq_sql)){
                                          $counter = $count += 1;
                                      ?>
                                        <option value="<?=$row['mq_id']?>">Column A, <?=$row['module_question']?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                  <div class="form-group col-4">
                                    <button class="btn btn-outline-primary btn-block" onclick="add_answer2(<?=$m_id?>)">Add Answer</button>
                                  </div>
                                </div>
                                <hr>
                                    <label>Column B</label>
                                <hr>
                                <?php
                                  $fetch_module_answer = mysqli_query($conn, "SELECT * FROM tbl_module_answer WHERE mq_id = '$m_id'");
                                  $count_ans = mysqli_num_rows($fetch_module_answer);
                                  $count2 = 1;
                                  if(mysqli_num_rows($fetch_module_answer) != 0){
                                    while ($ma_data = mysqli_fetch_array($fetch_module_answer)) {
                                ?>
                                <div class="row module_detail">
                                  <div class="form-group col-10">
                                    <label><?=$count2++?>. <?=$ma_data["module_answer"]?></label>
                                  </div>
                                  <div class="form-group col-1 pl-0">
                                    <button class="btn btn-danger btn-sm" onclick="remove_answer(<?=$ma_data['ma_id']?>)">Remove</button>
                                  </div>
                                </div>
                                <?php } }else{?>
                                  <tr>
                                    <td colspan="2" class="text-center">No data available</td>
                                  </tr>
                                <?php } ?>
                              </div>
                        </div>
                      <?php }  if(isset($row1["answer_type"]) && $row1["answer_type"] == 4){ ?>
                        <!-- DRAG N DROP GRAPHIC -->
                        <div class="row card-deck">
                          <div class="col-6 p-3 card ">
                        
                            <form id="add_mq_form3" method="POST" action="#" enctype="multipart/form-data">
                              <div class="row">
                                <div class="form-group col-12">
                                  <label>Add question</label>
                                  <input type="file" name="module_question" class="form-control" accept="image/png, image/gif, image/jpeg" >
                                  <input type="hidden" name="module_id" value="<?=$m_id?>">
                                </div>
                                <div class="form-group col-12">
                                  <button type="submit" class="btn btn-primary col-4 offset-8 btn-dd">Add Question</button>
                                </div>
                              </div>
                            </form>
                            <hr>
                            <label>Column A</label>
                            <hr>
                            <?php
                              $fetch_module_question = mysqli_query($conn, "SELECT * FROM tbl_module_question WHERE module_id = '$m_id' ORDER BY mq_id ASC");
                              if(mysqli_num_rows($fetch_module_question) != 0){
                                $count = 1;
                                $lastMqID = mysqli_fetch_array(mysqli_query($conn, "SELECT *, count(mq_id) as c FROM tbl_module_question WHERE module_id = '$m_id' ORDER BY mq_id DESC"));
                                while ($mq_data = mysqli_fetch_array($fetch_module_question)) {
                                $tag = $lastMqID["c"] == 1?1:($mq_data["mq_id"]+1) - $lastMqID["mq_id"];
                            ?>
                              <div class="row module_detail">
                                <div class="form-group col-10">
                                  <label><?=$tag?>.</label>
                                  <img src="<?=$mq_data["module_question"]?>" width="100" height="100" class="ml-3">
                                </div>
                                <div class="form-group col-1 pl-0">
                                  <button class="btn btn-danger btn-sm" onclick="remove_question(<?=$mq_data['mq_id']?>)">Remove</button>
                                </div>
                              </div>
                            <?php } }else{?>
                              <tr>
                                <td colspan="2" class="text-center">No data available</td>
                              </tr>
                            <?php } ?>
                            </div>
                            <div class="col-6 p-3 card ">
                              <label>Add answer</label>
                              <div class="row module_detail">
                                <div class="form-group col-12">
                                  <input type="file" id="ma_field3" class="form-control" accept="image/png, image/gif, image/jpeg" >
                                </div>
                                <label class="col-4 pl-0 text-right">Set as answer for:</label>
                                <div class="form-group col-4 pl-0">
                                  <select name="mq_answer_correct" class="form-control">
                                    <option value="">Select:</option>
                                    <?php
                                      $mq_sql = mysqli_query($conn, "SELECT * FROM tbl_module_question WHERE mq_id NOT IN (SELECT is_correct FROM tbl_module_answer) AND module_id = '$m_id' ORDER BY mq_id ASC") or die(mysqli_error($conn));
                                      $count = 1;
                                      $lastMaID = mysqli_fetch_array(mysqli_query($conn, "SELECT *, count(mq_id) as c FROM tbl_module_question WHERE module_id = '$m_id' ORDER BY mq_id DESC"));
                                      while($row = mysqli_fetch_array($mq_sql)){
                                        $tag1 = $lastMaID["c"] == 1?1:($row["mq_id"]+1) - $lastMaID["mq_id"];
                                    ?>
                                      <option value="<?=$row['mq_id']?>">Column A, <?=$tag1?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                                <div class="form-group col-4">
                                  <button class="btn btn-outline-primary btn-block btn-dd-a" onclick="add_answer3(<?=$m_id?>)">Add Answer</button>
                                </div>
                              </div>
                              <hr>
                                  <label>Column B</label>
                              <hr>
                              <?php
                                $fetch_module_answer = mysqli_query($conn, "SELECT * FROM tbl_module_answer WHERE mq_id = '$m_id'");
                                $count_ans = mysqli_num_rows($fetch_module_answer);
                                $count2 = 1;
                                if(mysqli_num_rows($fetch_module_answer) != 0){
                                  while ($ma_data = mysqli_fetch_array($fetch_module_answer)) {

                              ?>
                              <div class="row module_detail">
                                <div class="form-group col-10">
                                  <label><?=$count2++?>. </label>
                                  <img src="<?=$ma_data["module_answer"]?>" width="100" height="100" class="ml-3">
                                </div>
                                <div class="form-group col-1 pl-0">
                                  <button class="btn btn-danger btn-sm" onclick="remove_answer(<?=$ma_data['ma_id']?>)">Remove</button>
                                </div>
                              </div>
                              <?php } }else{?>
                                <tr>
                                  <td colspan="2" class="text-center">No data available</td>
                                </tr>
                              <?php } ?>
                            </div>
                      </div>

                      <?php } if(isset($fetch_module_question) && isset($count_ans) != 0 && $row1["content_type"] == 1 ){ ?>
                        <hr>
                        <div class="form-group col-2 offset-10">
                          <button type="button" class="btn btn-outline-success btn-block btn-post" onclick="post_module()">Post Module</button>
                        </div>
                      <?php } ?>
                  </div>
                    <!-- Learning Material -->
                    <div class="col-12 others" style="display: none;">
                      <hr>
                      <form id="others_form" class="row" method="POST" action="#" enctype="multipart/form-data">
                        <div class="form-group col-6 offset-3">
                          <label>Title</label>
                          <input type="text" name="module_name" class="form-control" placeholder="Material Name" <?=$mname?>>
                        </div>
                        <div class="form-group col-6 offset-3">
                          <label>Description</label>
                          <textarea name="desc" class="form-control" placeholder="Type here..." <?=$disabled?>><?=$lm_desc?></textarea>
                        </div>
                        <?php if($lm_desc == ""){?>
                          <div class="form-group col-6 offset-3">
                            <label>Attachment</label><br>
                            <input type="file" name="attachment[]" multiple="multiple">
                          </div>
                        <?php }else{ ?>
                          <div class="table-responsive col-6 offset-3">
                            <table class="table table-sm">
                              <thead>
                                <tr>
                                  <th>Attachment</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                  $getAttachment = mysqli_query($conn, "SELECT module_question FROM tbl_module_question WHERE module_id = '$m_id'");
                                  while($aRow = mysqli_fetch_array($getAttachment)){
                                ?>
                                  <tr>
                                    <td><a href="<?=$aRow[0]?>" target="_blank" download><?=$aRow[0]?></a></td>
                                  </tr>
                                <?php } ?>
                              </tbody>
                            </table>
                          </div>
                        <?php } ?>
                        <div class="col-2 offset-10">
                          <button type="submit" class="btn btn-primary btn-block btn-lm">Add</button>
                        </div>
                      </form>
                    </div>
                    
                    <!-- Activity -->

                    <div class="col-12 activity" style="display: none;">
                      <hr>
                      <form id="activity_form" class="row" method="POST" action="#">
                        <div class="form-group col-6 offset-3">
                          <label>Title</label>
                          <input type="text" name="module_name" class="form-control" placeholder="Activity Name" <?=$mname?>>
                        </div>
                        <div class="form-group col-6 offset-3">
                          <label>Description / Instructions</label>
                          <textarea name="desc" class="form-control" placeholder="Type here..." <?=$disabled?>><?=$lm_desc?></textarea>
                        </div>
                        <div class="form-group col-3 offset-3">
                            <label for="exampleInputEmail1">Module Deadline</label>
                            <input type="date" name="module_deadline" class="form-control" placeholder="Module Deadline" <?=$deadline?>>
                          </div>
                        <?php if(isset($fetch_module_question) && isset($row1["content_type"]) && $row1["content_type"] == 3){ ?>
                          <hr>
                          <div class="col-2 offset-10">
                            <button type="button" class="btn btn-outline-success btn-block" onclick="post_activity()">Post Activity</button>
                          </div>
                        <?php }else{ ?>
                          <div class="col-2 offset-10">
                            <button type="submit" class="btn btn-primary btn-block">Add</button>
                          </div>
                          <?php } ?>
                      </form>
                    </div>

                  </div>

                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
      </div>

    </section>

    <script type="text/javascript">
      $(document).ready( function(){
        select_content();
      });
      <?php if(isset($row1["is_posted"]) && $row1["is_posted"] == 1){?>
        $("input").prop("disabled", true);
        $("textarea").prop("disabled", true);
        $(".btn").prop("disabled", true);
        $("select").prop("disabled", true);
      // $(".btn-outline-success").prop("disabled", false);
      <?php }else{ echo "";}?>
      $("#add_module_form").submit( function(e){
        e.preventDefault();
        var c_type = $("#content_type").val();
        var data = $(this).serialize()+"&c_type="+c_type;
        var url = "../ajax/module_add.php";
        var s_id = "<?=$s_id?>";
        var ans_type = $("select[name=answer_type]").val();
        if(ans_type != 0){
          $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(data){
              if(data != 0){
                var c_id = "<?=$c_id?>";
                var pF = "<?=$pF?>";
                window.location.href="index.php?page=<?=page_url('subject_modules')?>&s_id="+s_id+"&m_id="+data+"&c_id="+c_id+"&pF="+pF;
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }else{
          alert("Warning: Please fill up required fields");
        }
      });

      $("#others_form").submit( function(e){
        e.preventDefault();
        var s_id = "<?=$s_id?>";
        var c_type = $("#content_type").val();
        var data = new FormData(this);
        data.append('c_type', c_type);
        data.append('s_id', s_id);
        var url = "../ajax/others_add.php";
        $(".btn-lm").prop("disabled", true);
        $(".btn-lm").html("<i class='fa fa-spinner fa-spin'></i> Loading...");
         
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          contentType: false,
          processData: false,
          success: function(data){
            if(data != 0){
              var c_id = "<?=$c_id?>";
              var pF = "<?=$pF?>";
              window.location.href="index.php?page=<?=page_url('subject_modules')?>&s_id="+s_id+"&m_id="+data+"&c_id="+c_id+"&pF="+pF;
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      $("#activity_form").submit( function(e){
        e.preventDefault();
        var s_id = "<?=$s_id?>";
        var c_type = $("#content_type").val();
        var data = $(this).serialize()+"&s_id="+s_id+"&c_type="+c_type;
        var url = "../ajax/activity_add.php";

        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data != 0){
              var c_id = "<?=$c_id?>";
              var pF = "<?=$pF?>";
              window.location.href="index.php?page=<?=page_url('subject_modules')?>&s_id="+s_id+"&m_id="+data+"&c_id="+c_id+"&pF="+pF;
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function select_content(){
        var c_type = $("#content_type").val();
        if(c_type == 1){
          $(".modules").show();
          $(".others").hide();
          $(".activity").hide();
        }else if(c_type == 2){
          $(".modules").hide();
          $(".others").show();
          $(".activity").hide();
        }else{
          $(".modules").hide();
          $(".others").hide();
          $(".activity").show();
        }
      }

      $("#add_mq_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/module_question_add.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              alert("Success: New question successfully added.");
              window.location.reload();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      $("#add_mq_form2").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/module_question_add.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              window.location.reload();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      $("#add_mq_form3").submit( function(e){
        e.preventDefault();
        var data = new FormData(this);
        data.append('ans_type', 3);
        var url = "../ajax/module_question_add.php";
        $(".btn-dd").prop("disabled", true);
        $(".btn-dd").html("<i class='fa fa-spinner fa-spin'></i> Loading...");

        $.ajax({
          type: "POST",
          url: url,
          data: data,
          contentType: false,
          processData: false,
          success: function(data){
            if(data == 1){
              window.location.reload();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function remove_question(mqID){
        var conf = confirm("Are you sure to remove this question?");
        if(conf){
          var url = "../ajax/module_question_delete.php";
          $.ajax({
            type: "POST",
            url: url,
            data: {mqID: mqID},
            success: function(data){
              if(data == 1){
                window.location.reload();
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }
      }

      function add_answer(mqID){
        var ans = $("#ma_field"+mqID).val();
        var cb_ans = $("#cb_ans"+mqID).is(":checked")?1:0;
        var url = "../ajax/module_answer_add.php";
        if(ans != ""){
          $.ajax({
            type: "POST",
            url: url,
            data: {mqID: mqID, answer: ans, is_correct: cb_ans},
            success: function(data){
              if(data == 1){
                window.location.reload();
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }else{
          alert("Notice: Please fill up required fields.");
        }
      }

      function add_answer2(mqID){
        var ans = $("#ma_field2").val();
        var cb_ans = $("select[name=mq_answer_correct]").val();
        var url = "../ajax/module_answer_add.php";
        if(ans != "" && cb_ans != 0){
          $.ajax({
            type: "POST",
            url: url,
            data: {mqID: mqID, answer: ans, is_correct: cb_ans, ans_type: 3},
            success: function(data){
              if(data == 1){
                window.location.reload();
              }else if(data == 2){
                alert("Warning: Correct answer has been already added to a question");
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }else{
          alert("Notice: Please fill up required fields.");
        }
      }

      function add_answer3(mqID){
        var ans = document.getElementById("ma_field3").files[0];
        var cb_ans = $("select[name=mq_answer_correct]").val();
        var url = "../ajax/module_answer_add.php";
        $(".btn-dd-a").prop("disabled", true);
        $(".btn-dd-a").html("<i class='fa fa-spinner fa-spin'></i> Loading...");

        var data = new FormData();
        data.append("mqID", mqID);
        data.append("answer", ans);
        data.append("is_correct", cb_ans);
        data.append("ans_type", 4);

        if(ans != "" && cb_ans != 0){
          $.ajax({
            type: "POST",
            url: url,
            data: data,
            contentType: false,
            processData: false,
            success: function(data){
              if(data == 1){
                window.location.reload();
              }else if(data == 2){
                alert("Warning: Correct answer has been already added to a question");
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }else{
          alert("Notice: Please fill up required fields.");
        }
      }

      function remove_answer(maID){
        var conf = confirm("Are you sure to remove this answer?");
        if(conf){
          var url = "../ajax/module_answer_delete.php";
          $.ajax({
            type: "POST",
            url: url,
            data: {maID: maID},
            success: function(data){
              if(data == 1){
                window.location.reload();
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }
      }

      function post_module(){
        var m_id = "<?=isset($m_id)?$m_id:"0"?>";
        var conf = confirm("Are you sure to post this module?");
        if(conf){
          $(".btn-post").prop("disabled", true);
          $(".btn-post").html("<i class='fa fa-spinner fa-spin'></i> Loading");

          var url = "../ajax/module_post.php";
          $.ajax({
            type: "POST",
            url: url,
            data: {post: 1, m_id: m_id},
            success: function(data){
              if(data != 0){
                alert("Success: Module is now posted.");
                window.location.reload();
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }
      }

      function post_activity(){
        var m_id = "<?=isset($m_id)?$m_id:"0"?>";
        var conf = confirm("Are you sure to post this activity?");
        if(conf){
          var url = "../ajax/module_post.php";
          $.ajax({
            type: "POST",
            url: url,
            data: {post: 1, m_id: m_id},
            success: function(data){
              if(data == 1){
                alert("Success: Activity is now posted.");
                window.location.reload();
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }
      }
    </script>