<?php
  $s_id = $_GET["s_id"];
  $c_id = $_GET["c_id"];
  $pF = $_GET["pF"];
  $row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_subject WHERE subject_id = '$s_id'"));

  if(isset($_GET["m_id"])){
    $m_id = $_GET["m_id"];
    $row1 = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_modules WHERE module_id = '$m_id'"));

    $show_details = $row1["answer_type"] == 1 || $row1["answer_type"] == 2?"":"display:none;";
    $show_details2 = $row1["answer_type"] == 1 || $row1["answer_type"] == 2?"display:none;":"";
    $mname = isset($row1["module_name"])?"value='".$row1["module_name"]."' readonly":"";
    $deadline = isset($row1["module_deadline"])?"value='".$row1["module_deadline"]."' readonly":"";
    $a_type = isset($row1["answer_type"]) && $row1["answer_type"] != 0?"disabled":"";
    $disabled = isset($row1["module_id"])?"disabled":"";
    $overdue = date("Y-m-d", strtotime($row1["module_deadline"])) < date("Y-m-d")?"disabled":"";
    $module_overdue = date("Y-m-d", strtotime($row1["module_deadline"])) < date("Y-m-d")?"<span class='text-danger'> ( Expired )</span>":"";
    $hide_m = $row1["content_type"] == 1?"":"style='display: none;'";
    $hide_o = $row1["content_type"] == 2?"":"style='display: none;'";
    $hide_a = $row1["content_type"] == 3?"":"style='display: none;'";
    $content_type = $row1["content_type"] == 1?"Module":($row1["content_type"] == 2?"Material":"Activity");

  }

  $has_answered = mysqli_num_rows(mysqli_query($conn, "SELECT s_answer_id FROM tbl_student_answers WHERE user_id = '$_SESSION[uid]' AND module_id = '$m_id' GROUP BY module_id"));
?>
<style type="text/css">
  .non-draggable {
      user-drag: none !important;
      -webkit-user-drag: none !important;
      user-select: none !important;
      -moz-user-select: none !important;
      -webkit-user-select: none !important;
      -ms-user-select: none !important;
  }
</style>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1><a href="index.php?page=<?=page_url('subject_details')?>&s_id=<?=$s_id?>&c_id=<?=$c_id?>&pF=<?=$pF?>"><i class="fa fa-chevron-left"></i> Subject</a> / <?=$content_type?> details</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <!-- Module -->
          <div class="row" <?=$hide_m?>>
            <div class="col-12 mb-3">
              <h1>Module: <?=strtoupper($row1["module_name"])?> <?=$module_overdue?></h1>
            </div>
            <div class="col-md-12">
              <?php if($row1["content_type"] == 1){if($has_answered == 0){ ?>
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title">Modules Questions</h5>
                  <!-- <div class="card-tools">
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_module_md">
                      Add
                    </button>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_module()">
                      Delete
                    </button>
                  </div> -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <label>Instructions:</label>
                  <p class="col-10 offset-1">
                    <?=$row1["module_instructions"]?>
                  </p>
                  <hr>
                  <form id="module_form" class="row">
                    <div class="col-10 offset-1">

                      <!-- question -->
                      <div class="row" style="<?=$show_details?>">
                        <?php
                          $fetch_module_question = mysqli_query($conn, "SELECT * FROM tbl_module_question WHERE module_id = '$m_id'");
                          if($fetch_module_question){
                            $count_q = 1;
                            while ($mq_data = mysqli_fetch_array($fetch_module_question)) {
                                $c_question = $count_q++;
                        ?>
                          <div class="col-12 p-3 card">
                            <div class="row module_detail">
                              <div class="form-group col-12">
                                <label><?=$c_question?>. <?=$mq_data["module_question"]?></label>
                                <input type="hidden" class="qID" value="<?=$mq_data['mq_id']?>">
                              </div>
                              <hr>

                              <?php
                                  $fetch_module_answer = mysqli_query($conn, "SELECT * FROM tbl_module_answer WHERE mq_id = '$mq_data[mq_id]'");
                                  $count_a = 1;
                                ?>
                                <div class="col-12">
                                    <?php 
                                      if($row1["answer_type"] == 1){
                                        while ($ma_data = mysqli_fetch_array($fetch_module_answer)) {
                                          $c_answer = $count_a++;
                                    ?>
                                      <label class="col-12">
                                          <input type="radio" value="<?=$ma_data['ma_id']?>" class="mr-1 q_ans q<?=$c_question?> q<?=$c_question?>_a<?=$c_answer?>" onclick="on_answer(<?=$c_question?>,<?=$c_answer?>)"> <?=$ma_data['module_answer']?>
                                      </label>
                                    <?php
                                      } }else{ 
                                    ?>
                                      <select class="form-control col-3 q_ans">
                                        <option value="">Select Answer:</option>
                                    <?php
                                        while ($ma_data = mysqli_fetch_array($fetch_module_answer)) {
                                        $c_answer = $count_a++;
                                    ?>
                                        <option value="<?=$ma_data['ma_id']?>"><?=$ma_data['module_answer']?></option>
                                    <?php } ?>
                                    </select>
                                  <?php } ?>
                                </div>

                            </div>
                          </div>
                        <?php } } ?>

                      </div>

                      <?php if($row1["answer_type"] == 3){ ?>
                        <!-- DRAG N DROP -->

                          <!-- QUESTION -->
                          
                          <div class="row card-deck mb-3" id="dd-q" style="<?=$show_details2?>">
                            <div class="col-12 p-3 card">
                              <label>Column A</label>
                              <hr>
                              <div class="row">
                                <?php
                                  $fetch_module_question = mysqli_query($conn, "SELECT * FROM tbl_module_question WHERE module_id = '$m_id'");
                                  if($fetch_module_question){
                                    $count_q = 1;
                                    while ($mq_data = mysqli_fetch_array($fetch_module_question)) {
                                        $c_question = $count_q++;
                                ?>    
                                      <div class="col-4">
                                        <div class="card text-center pt-2 mb-3">
                                          <label><?=$mq_data["module_question"]?></label>
                                          <!-- <label class="text-muted">Place Here</label> -->
                                          <input type="hidden" name="ddq<?=$c_question?>" value="<?=$mq_data['mq_id']?>">
                                        </div>
                                        <div class="row" id="ddq<?=$c_question?>" ondrop="drop(event)" ondragover="allowDrop(event)" style="height: 100px; border: 1px solid;">
                                        </div>
                                      </div>
                                <?php } } ?>
                              </div>
                            </div>
                          </div>

                          <!-- ANSWER -->
                          <div class="col-12 p-3 card" id="dd-ans" style="<?=$show_details2?>">
                            <div class="row">
                              <label class="col-3">Column B</label>
                              <button type="button" class="btn btn-outline-success btn-sm col-1 offset-8" onclick="reset1()"><i class="fa fa-redo mr-1"></i> Reset</button>
                            </div>
                            <hr>
                            <div class="row card-deck">
                              <?php
                                $fetch_module_answer = mysqli_query($conn, "SELECT * FROM tbl_module_answer WHERE mq_id = '$m_id'");
                                $count_a = 1;
                                while ($ma_data = mysqli_fetch_array($fetch_module_answer)) {
                                $c_answer = $count_a++;
                              ?>
                                <div class="card text-center mb-3 col-4 p-2 draggable-ans" id="drag<?=$c_answer?>" draggable="true" ondragstart="drag(event)" style="cursor: move; height: 83px;">
                                  <?=$ma_data['module_answer']?>
                                  <input type="hidden" name="drag<?=$c_answer?>" value="<?=$ma_data['ma_id']?>">
                                </div>
                              <?php } ?>
                            </div>
                          </div>
                      <?php }else{ ?>
                        <!-- DRAG N DROP GRAPHIC -->

                          <!-- QUESTION -->
                          
                          <div class="row card-deck mb-3" id="dd-q" style="<?=$show_details2?>">
                            <div class="col-12 p-3 card">
                              <label>Column A</label>
                              <hr>
                              <div class="row">
                                <?php
                                  $fetch_module_question = mysqli_query($conn, "SELECT * FROM tbl_module_question WHERE module_id = '$m_id'");
                                  if($fetch_module_question){
                                    $count_q = 1;
                                    while ($mq_data = mysqli_fetch_array($fetch_module_question)) {
                                        $c_question = $count_q++;
                                ?>    
                                      <div class="col-4">
                                        <div class="card text-center pt-2 mb-3 row">
                                          <center>
                                            <img src="<?=$mq_data["module_question"]?>" width="200" height="100" class="mb-1">
                                          </center>
                                          <!-- <label class="text-muted">Place Here</label> -->
                                          <input type="hidden" name="ddq<?=$c_question?>" value="<?=$mq_data['mq_id']?>">
                                        </div>
                                        <div class="row" id="ddq<?=$c_question?>" ondrop="drop(event)" ondragover="allowDrop(event)" style="height: 130px; border: 1px solid;">
                                        </div>
                                      </div>
                                <?php } } ?>
                              </div>
                            </div>
                          </div>

                          <!-- ANSWER -->
                          <div class="col-12 p-3 card" id="dd-ans" style="<?=$show_details2?>">
                            <div class="row">
                              <label class="col-3">Column B</label>
                              <button type="button" class="btn btn-outline-success btn-sm col-1 offset-8" onclick="reset1()"><i class="fa fa-redo mr-1"></i> Reset</button>
                            </div>
                            <hr>
                            <div class="row card-deck">
                              <?php
                                $fetch_module_answer = mysqli_query($conn, "SELECT * FROM tbl_module_answer WHERE mq_id = '$m_id'");
                                $count_a = 1;
                                while ($ma_data = mysqli_fetch_array($fetch_module_answer)) {
                                $c_answer = $count_a++;
                              ?>
                                <div class="card text-center mb-3 col-4 p-2 draggable-ans" id="drag<?=$c_answer?>" draggable="true" ondragstart="drag(event)" style="cursor: move; height: 115px;">
                                  <center>
                                    <img src="<?=$ma_data['module_answer']?>" width="200" height="100" class="mb-1 non-draggable">
                                  </center>
                                  <input type="hidden" name="drag<?=$c_answer?>" value="<?=$ma_data['ma_id']?>">
                                </div>
                              <?php } ?>
                            </div>
                          </div>

                      <?php } ?>

                        <input type="hidden" value="<?=$m_id?>" name="m_id">
                        <button type="submit" class="btn btn-primary col-2 offset-10" <?=$overdue?>>Submit</button>
                    </form>
                  </div>

                </div>
              </div>
              <?php }else{ ?>
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title">Result</h5>
                </div>
                <!-- /.card-header -->
                <?php
                  if($row1["answer_type"] == 3 || $row1["answer_type"] == 4){
                    $ans_data = mysqli_fetch_array(mysqli_query($conn, "SELECT *, count(*) as total_q FROM tbl_student_answers WHERE module_id = '$m_id' AND user_id = '$_SESSION[uid]'"));
                    $correct_ans = mysqli_num_rows(mysqli_query($conn, "SELECT sa.s_answer_id FROM tbl_student_answers sa INNER JOIN tbl_module_answer ma ON ma.ma_id = sa.ma_id INNER JOIN tbl_module_question mq ON mq.module_id = sa.module_id WHERE sa.module_id = '$m_id' AND sa.user_id = '$_SESSION[uid]' AND ma.is_correct = sa.mq_id GROUP BY sa.s_answer_id"));
                    $times = ($correct_ans * $ans_data["total_q"]);
                    $pctg = $correct_ans == 0?0:($correct_ans/$ans_data["total_q"])*100;
                  }else{
                    $ans_data = mysqli_fetch_array(mysqli_query($conn, "SELECT *, count(*) as total_q FROM tbl_student_answers WHERE module_id = '$m_id' AND user_id = '$_SESSION[uid]'"));
                    $correct_ans = mysqli_num_rows(mysqli_query($conn, "SELECT sa.s_answer_id FROM tbl_student_answers sa INNER JOIN tbl_module_answer ma ON ma.ma_id = sa.ma_id WHERE sa.module_id = '$m_id' AND sa.user_id = '$_SESSION[uid]' AND ma.is_correct = 1"));
                    $times = ($correct_ans * $ans_data["total_q"]);
                    $pctg = $correct_ans == 0?0:($correct_ans/$ans_data["total_q"])*100;
                  }
                ?>
                <div class="card-body">
                  <div class="col-12 text-center">
                    <?php if($pctg >= 50){?>
                      <h1>
                        Congratulations<br>
                        <?=$pctg?>%<br>
                        Very Good<br>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                      </h1>
                    <?php }else{ ?>
                      <h1>
                        Sorry<br>
                        <?=$pctg?>%<br>
                        You failed<br>
                        <i class="far fa-frown"></i>
                      </h1>
                    <?php } ?>
                    
                    <div class="col-2 offset-5 mt-3">
                      <a href="index.php?page=<?=page_url('subject_details')?>&s_id=<?=$s_id?>&c_id=<?=$c_id?>&pF=<?=$pF?>" class="btn btn-primary btn-block">Okay</a>
                    </div>
                  </div>
                </div>
              </div>
              <?php } } ?>
              <!-- /.card -->
            </div>
              <!-- /.col -->
          </div>
          

          <!-- Other -->
          <div class="row" <?=$hide_o?>>
              <div class="col-12 mb-3">
                <h1>Learning Material: <?=strtoupper($row1["module_name"])?></h1>
              </div>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title">Details</h5>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="col-12">
                      <label>Instructions</label>
                      <p><?=$row1["module_instructions"]?></p>
                      <hr>
                    </div>
                    <div class="col-12">
                      <div class="row card-deck">
                        <?php
                          if($row1["content_type"] == 2){
                            $getAttachment = mysqli_query($conn, "SELECT module_question FROM tbl_module_question WHERE module_id = '$m_id'");
                            while($aRow = mysqli_fetch_array($getAttachment)){
                              $aName = explode("/", $aRow[0]);
                        ?>
                          <div class="col-3 card text-center p-2">
                            <h1><i class="fa fa-file-alt"></i></h1>
                            <a href="<?=$aRow[0]?>" target="_blank" download><?=$aName[3]?></a>
                          </div>
                        <?php } } ?>
                      <div>
                    </div>

                  </div>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
            </div>
          </div>

          <!-- Activity -->
          <div class="row" <?=$hide_a?>>
              <div class="col-12 mb-3">
                <h1>Activity: <?=strtoupper($row1["module_name"])?> <?=$module_overdue?></h1>
              </div>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title">Details</h5>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body row">
                    <div class="col-6">
                      <label>Instructions</label>
                      <pre><?=$row1["module_instructions"]?></pre>
                    </div>
                    <div class="col-6">
                      <form id="activity_form" method="post" action="#" enctype="multipart/form-data">
                        <div class="form-group">
                          <label>Upload Activity</label><br>
                          <input type="file" name="activity[]" multiple="multiple" required>
                          <input type="hidden" name="m_id" value="<?=$m_id?>">
                        <div>
                        <div class="form-group pt-3">
                          <button type="submit" class="btn btn-primary col-2 offset-10">Add</button>
                        </div>
                      </form>
                    </div>
                    
                    <div class="col-12">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Filename</th>
                            <th width="15px"></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                            $getAnswer = mysqli_query($conn, "SELECT * FROM tbl_module_answer WHERE mq_id = '$m_id' AND is_correct = '$_SESSION[uid]'");
                            if(mysqli_num_rows($getAnswer) != 0){
                            while($row = mysqli_fetch_array($getAnswer)){
                              $ans = explode("/", $row["module_answer"]);
                          ?>
                            <tr>
                              <td><a href="<?=$row['module_answer']?>" download><u><?=$ans[3]?></u></a></td>
                              <td><button type="button" class="btn btn-outline-danger" onclick="delete_file(<?=$row['ma_id']?>)"><i class="fa fa-trash-alt"></i></button></td>
                            </tr>
                          <?php } }else{ ?>
                            <tr>
                              <td colspan="2">No data available</td>
                            </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h5 class="card-title">Comments</h5>
          </div>
          <div class="card-body">
            <div class="col-12">
              <div class="card-footer card-comments">
                <?php
                  $getComment = mysqli_query($conn, "SELECT * FROM tbl_comment WHERE module_id = '$m_id'");
                  if(mysqli_num_rows($getComment) != 0){
                    while($row = mysqli_fetch_array($getComment)){
                ?>
                <div class="card-comment">
                  <!-- User image -->
                  <h1><i class="fa fa-user-circle img-circle img-sm"></i></h1>

                  <div class="comment-text pl-2">
                    <span class="username">
                      <?=strtoupper(getStudentName($conn, $row["added_by"]))?>
                      <button type="button" class="close float-right ml-3" data-dismiss="modal" aria-label="Close" onclick="delete_comment(<?=$row['comment_id']?>)">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <span class="text-muted float-right pt-2"><?=date("F d, Y", strtotime($row["date_added"]))?></span>
                    </span><!-- /.username -->
                    <?=$row["comment"]?>
                  </div>
                  <!-- /.comment-text -->
                </div>
                <?php } }?>
              </div>
              <div class="card-footer">
                <form id="comment_form" action="#" method="post" autocomplete="false">
                  <!-- <img class="img-fluid img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="Alt Text"> -->
                  <!-- .img-push is used to add margin to elements next to floating images -->
                  <div class="img-push">
                    <input type="text" name="comments" class="form-control form-control-sm" placeholder="Press enter to post comment" autofocus="true">
                    <input type="hidden" name="m_id" value="<?=$m_id?>">
                  </div>
                </form>
              </div>
            </div>
          </div>

        </div>
      </div>

    </section>

    <script type="text/javascript">
      $(document).ready( function(){
        <?php
          if(date("Y-m-d", strtotime($row1["module_deadline"])) < date("Y-m-d")){
        ?>
          $("input").prop("disabled", true);
          $("button").prop("disabled", true);
          $("input[name=comments]").prop("disabled", false);
          $("input[name=m_id]").prop("disabled", false);
          $(".close").prop("disabled", false);
        <?php }?>
      });

      function on_answer(qOrder, aOrder){
          $(".q"+qOrder).prop("checked", false);
          $(".q"+qOrder+"_a"+aOrder).prop("checked", true);
      }

      $("#comment_form").submit( function(e){
        e.preventDefault();        var data = $(this).serialize();
        var url = "../ajax/comment_add.php";

        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data != 0){
              window.location.reload();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      $("#module_form").submit( function(e){
        e.preventDefault();
        var answers = [];
        var questions = [];

        <?php if($row1["answer_type"] == 1){?>
          $(".q_ans:checked").each( function(){
            answers.push($(this).val());
          });
        <?php }else if($row1["answer_type"] == 2){ ?>
          $(".q_ans").each( function(){
            answers.push($(this).val());
          });
        <?php }else{ ?>
          answers.push("true");
        <?php } ?>
        


        $(".qID").each( function(){
          questions.push($(this).val());
        });
        
        <?php if($row1["answer_type"] == 3 || $row1["answer_type"] == 4){?>
          var data = $(this).serialize()+"&arr_a="+arr_a+"&arr_q="+arr_q;
        <?php }else{ ?>
          var data = $(this).serialize()+"&answers="+answers+"&questions="+questions;
        <?php } ?>

        var url = "../ajax/student_answer_add.php";
        var conf = confirm("Are you sure to submit module?");

        if(answers.length != 0){
          if(conf){
            $.ajax({
              type: "POST",
              url: url,
              data: data,
              success: function(data){
                if(data != 0){
                  alert("Success: Module was submitted!");
                  window.location.reload();
                }else{
                  alert("Error: Something is wrong.");
                }
              }
            });
          }
        }else{
          alert("Notice: Answers cannot be blank.");
        }
      });

      $("#activity_form").submit( function(e){
        e.preventDefault();
        var data = new FormData(this);
        var url = "../ajax/activity_answer_add.php";
         
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          contentType: false,
          processData: false,
          success: function(data){
            if(data != 0){
              alert("Success: File/s was uploaded");
              window.location.reload();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function delete_file(aID){
        var url = "../ajax/activity_answer_delete.php";
        var conf = confirm("Are you sure to delete?");
        if(conf){
          $.ajax({
            type: "POST",
            url: url,
            data: {aID: aID},
            success: function(data){
              if(data == 1){
                window.location.reload();
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }
      }

      function delete_comment(cID){
        var url = "../ajax/comment_delete.php";
        var conf = confirm("Are you sure to delete?");
        if(conf){
          $.ajax({
            type: "POST",
            url: url,
            data: {cID: cID},
            success: function(data){
              if(data == 1){
                window.location.reload();
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }
      }

      //DRAG N DROP
      function allowDrop(ev) {
        ev.preventDefault();
      }

      function drag(ev) {
        ev.dataTransfer.setData("text", ev.target.id);
      }

      var arr_q = [];
      var arr_a = [];

      function drop(ev) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        ev.target.append(document.getElementById(data));
        var id = document.getElementById(data);
        var targetID = ev.target.id;
        $(id).addClass("bg-secondary col-12 mt-2");
        var ma_id = $("input[name="+data+"]").val();
        var mq_id = $("input[name="+targetID+"]").val();
        // add this line
        $(id).attr('ondragstart', false);
        $(id).attr('draggable', false);
        $("#"+targetID).attr('ondrop', "");
        $("#"+targetID).attr('ondragover', "");
        $(id).css('cursor', 'initial');

        arr_q.push(mq_id);
        arr_a.push(ma_id);
      }

      $('#dd-ans').data('old-state', $('#dd-ans').html());
      $('#dd-q').data('old-state1', $('#dd-q').html());

      function reset1(){  
        $('#dd-ans').html($('#dd-ans').data('old-state'));
        $('#dd-q').html($('#dd-q').data('old-state1'));

        arr_q = [];
        arr_a = [];
      }

    </script>