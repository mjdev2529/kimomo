<?php
  $c_id = $_GET["c_id"];
  $pF = $_GET["pF"];

  $row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_classes WHERE class_id = '$c_id'"));
  $hide_s = $_SESSION['role'] == 0 || $pF == 'modules'?"style='display:none;'":"";
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <?php if($pF == "classes"){?>
              <h1><a href="index.php?page=<?=page_url('classes')?>"><i class="fa fa-chevron-left"></i> Classes</a> / Class details</h1>
            <?php }else{ ?>
              <h1><a href="index.php?page=<?=page_url('modules')?>"><i class="fa fa-chevron-left"></i> Modules</a> / Class details</h1>
            <?php } ?>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-6 mb-3">
              <h1>Class: <?=$row["class_name"]?></h1>
            </div>            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title">Subjects List</h5>
                  <div class="card-tools" <?=$hide_s?>>
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_subject_md">
                      Add
                    </button>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_class()">
                      Delete
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="tbl_subject" class="table table-condensed ">
                    <thead>
                      <tr>
                        <th style="width: 10px"><input type="checkbox" id="checkAllClass" onclick="checkAllClass()"></th>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th style="width: 100px">Quarter</th>
                        <th style="width: 100px">Date Added</th>
                        <th style="width: 100px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <!-- Add Modal -->
    <div class="modal fade" id="add_subject_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Subject</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="add_subject_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Subject Name</label>
                  <input type="text" name="subject_name" class="form-control" placeholder="Subject Name">
                  <input type="hidden" name="class_id" value="<?=$c_id?>">
                </div>
                <!-- <div class="form-group">
                  <label for="exampleInputEmail1">Quarter</label>
                  <select name="subject_qtr" class="form-control">
                    <option value="0">Select:</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select>
                </div> -->
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="edit_subject_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Subject</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="edit_subject_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Subject Name</label>
                  <input type="text" name="subject_name" id="subject_name" class="form-control" placeholder="Subject Name">
                  <input type="hidden" name="class_id" value="<?=$c_id?>">
                  <input type="hidden" name="subject_id" id="subject_id" >
                </div>
                <!-- <div class="form-group">
                  <label for="exampleInputEmail1">Quarter</label>
                  <select name="subject_qtr" id="subject_qtr" class="form-control">
                    <option value="0">Select:</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select>
                </div> -->
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $(document).ready( function(){
        get_subject();
      });

      function get_subject(){
        $("#tbl_subject").DataTable().destroy();
        $("#tbl_subject").dataTable({
          "ajax": {
            "type": "POST",
            "url": "../ajax/datatables/subjects_data.php",
            "data": {c_id: "<?=$c_id?>"}
          },
          "processing": true,
          "bPaginate": false,
          "bLengthChange": false,
          "bFilter": true,
          "bInfo": false,
          "sort": false,
          "columns": [
          {
            "mRender": function(data, type, row){
              return "<input type='checkbox' value='"+row.subject_id+"' name='cb_subject'>";
            }
          },
          {
            "data": "count"
          },
          {
            "data": "subject_name"
          },
          {
            "data": "quarter"
          },
          {
            "data": "date_added"
          },
          {
            "mRender": function(data, type, row){
              return "<button class='btn btn-sm btn-outline-dark btn-block' onclick='edit_subject("+row.subject_id+")' <?=$hide_s?>>Edit data</button>"+
              "<button class='btn btn-sm btn-outline-dark btn-block' onclick='subject_details("+row.subject_id+")'>View</button>";
            }
          }
          ]
        });
      }

      function checkAllClass(){
        var x = $("#checkAllClass").is(":checked");
        if(x){
          $("input[name=cb_classes]").prop("checked", true);
        }else{
          $("input[name=cb_classes]").prop("checked", false);
        }
      }

      $("#add_subject_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/subject_add.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              alert("Success: New subject was added.");
              $("#add_subject_md").modal("hide");
              $("input").val("");
              get_subject();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function edit_subject(sID){
        var url = "../ajax/subject_details.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {sID: sID},
          success: function(data){
            $("#edit_subject_md").modal();
            var o = JSON.parse(data);
            $("#subject_id").val(sID);
            $("#subject_name").val(o.subject_name);
            // $("#subject_qtr").val(o.quarter);
          }
        });
      }

      $("#edit_subject_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/subject_update.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              alert("Success: Subject details was updated.");
              $("#edit_subject_md").modal("hide");
              $("input").val("");
              get_subject();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function delete_class(){
        var conf = confirm("Are you sure to delete selected?");
        if(conf){
          var subject = [];
          $("input[name=cb_subject]:checked").each( function(){
            subject.push($(this).val());
          });

          if(subject.length != 0){
            var url = "../ajax/subject_delete.php";
            $.ajax({
              type: "POST",
              url: url,
              data: {sID: subject},
              success: function(data){
                if(data != 0){
                  alert("Success: Selected subject/s was removed.");
                  get_subject();
                }else{
                  alert("Error: Something is wrong.");
                }
              }
            });
          }else{
            alert("Warning: No data selected.");
          }
        }
      }

      function subject_details(sID){
        var c_id = "<?=$c_id?>";
        var pF = "<?=$pF?>";
        if(pF == "classes"){
          window.location.href="index.php?page=<?=page_url('subject_details')?>&c_id="+c_id+"&s_id="+sID+"&pF="+pF;
        }else{
          window.location.href="index.php?page=<?=page_url('modules_add')?>&c_id="+c_id+"&s_id="+sID+"&pF="+pF;
        }
      }
    </script>