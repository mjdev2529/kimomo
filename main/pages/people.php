<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1><?=$_SESSION['role'] == 1?"Students":"My Classmates"?></h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
          <?php if($_SESSION["role"] == 1){ ?>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title">Classes List</h5>
                    <!-- <div class="card-tools">
                      <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_class_md">
                        Add
                      </button>
                      <button type="button" class="btn btn-sm btn-danger" onclick="delete_class()">
                        Delete
                      </button>
                    </div> -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="tbl_class" class="table table-condensed ">
                      <thead>
                        <tr>
                          <!-- <th style="width: 10px"><input type="checkbox" id="checkAllClass" onclick="checkAllClass()"></th> -->
                          <th style="width: 10px">#</th>
                          <th>Name</th>
                          <th style="width: 100px">Code</th>
                          <th style="width: 100px">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card -->
              </div>
            <?php }else{ ?>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title">Classes List</h5>
                    <!-- <div class="card-tools">
                      <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_st_class_md">
                        Add
                      </button>
                      <button type="button" class="btn btn-sm btn-danger" onclick="delete_student_class()">
                        Delete
                      </button>
                    </div> -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="tbl_class_student" class="table table-condensed ">
                      <thead>
                        <tr>
                          <!-- <th style="width: 10px"><input type="checkbox" id="checkAllClass" onclick="checkAllClass()"></th> -->
                          <th style="width: 10px">#</th>
                          <th>Name</th>
                          <th width="300px">Teacher Name</th>
                          <th style="width: 100px">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card -->
              </div>
            <!-- /.col -->
            <?php } ?>
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <!-- People modal -->
    <div class="modal fade" id="view_people_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> <?=$_SESSION['role'] == 1?"Students":"My Classmates"?> List</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body people-md-body">
                
          </div>
        </div>
      </div>
    </div>

    <!-- Profile modal -->
    <div class="modal fade" id="view_profile_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Profile Information</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body profile-md-body">
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">
              Close
            </button>
          </div>
        </div>
      </div>
    </div>


    <script type="text/javascript">
      $(document).ready( function(){
        get_class();
        get_class_student();
      });

      //teacher 
      function get_class(){
        $("#tbl_class").DataTable().destroy();
        $("#tbl_class").dataTable({
          "ajax": {
            "type": "POST",
            "url": "../ajax/datatables/classes_data.php",
          },
          "processing": true,
          "bPaginate": false,
          "bLengthChange": false,
          "bFilter": true,
          "bInfo": false,
          "sort": false,
          "columns": [
        //   {
        //     "mRender": function(data, type, row){
        //       return "<input type='checkbox' value='"+row.class_id+"' name='cb_classes'>";
        //     }
        //   },
          {
            "data": "count"
          },
          {
            "data": "class_name"
          },
          {
            "data": "class_code"
          },
          {
            "mRender": function(data, type, row){
              return "<button class='btn btn-sm btn-outline-dark btn-block' onclick='view_people("+row.class_id+")'>View students</button>";
            }
          }
          ]
        });
      }

      //student
      function get_class_student(){
        $("#tbl_class_student").DataTable().destroy();
        $("#tbl_class_student").dataTable({
          "ajax": {
            "type": "POST",
            "url": "../ajax/datatables/classes_student_data.php",
          },
          "processing": true,
          "bPaginate": false,
          "bLengthChange": false,
          "bFilter": true,
          "bInfo": false,
          "sort": false,
          "columns": [
          {
            "data": "count"
          },
          {
            "data": "class_name"
          },
          {
            "data": "teacher_name"
          },
          {
            "mRender": function(data, type, row){
              return "<button class='btn btn-sm btn-outline-dark btn-block' onclick='view_people("+row.class_id+")'>View details</button>";
            }
          }
          ]
        });
      }

      function view_people(cID){
        var url = "../ajax/class_view_people.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {cID: cID},
          success: function(data){
            if(data){
                $("#view_people_md").modal();
                $(".people-md-body").html(data);
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      }

      function viewProfile(uID){
        var url = "../ajax/user_view_profile.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {uID: uID},
          success: function(data){
            if(data){
              $("#view_profile_md").modal();
              $(".profile-md-body").html(data);
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      }
    </script>