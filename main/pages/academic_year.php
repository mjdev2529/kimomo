<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1>Academic year</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
                  <table id="tbl_academic_year" class="table">
                    <thead>
                      <tr>
                        <!-- <th style="width: 10px"><input type="checkbox" id="checkAllClass" onclick="checkAllClass()"></th> -->
                        <th style="width: 10px">#</th>
                        <th>Year</th>
                        <th style="width: 100px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <script type="text/javascript">
      $(document).ready( function(){
        get_academic();
      });

      function get_academic(){
        $("#tbl_academic_year").DataTable().destroy();
        $("#tbl_academic_year").dataTable({
          "ajax": {
            "type": "POST",
            "url": "../ajax/datatables/academic_year_data.php",
          },
          "processing": true,
          "bPaginate": false,
          "bLengthChange": false,
          "bFilter": true,
          "bInfo": false,
          "sort": false,
          "columns": [
          {
            "data": "count"
          },
          {
            "data": "year"
          },
          {
            "mRender": function(data, type, row){
              return "<button class='btn btn-sm btn-outline-dark' onclick='ayear_details()'>View Details</button>";
            }
          }
          ]
        });
      }

      function ayear_details(){
        window.location.href="index.php?page=<?=page_url('academic_year_details')?>";
      }
    </script>