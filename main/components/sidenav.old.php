<?php
  if($page == page_url('dashboard')){
    $dashboard = "active";
    $sales = "";
    $products = "";
  }else if($page == page_url('sales')){
    $dashboard = "";
    $sales = "active";
    $products = "";
  }else if($page == page_url('products')){
    $dashboard = "";
    $sales = "";
    $products = "active";
  }else if($page == page_url('suppliers')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "active";
  }
?>
 <div class="sidebar-sticky pt-3">
  	<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>navigation</span>
    </h6>
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link h6 <?=$dashboard?>" href="index.php?page=<?=page_url('dashboard')?>">
          <span class="fa fa-home"></span>
          Dashboard <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link h6 <?=$sales?>" href="index.php?page=<?=page_url('sales')?>">
          <span class="fa fa-shopping-cart"></span>
          Sales
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link h6 <?=$products?>" href="index.php?page=<?=page_url('products')?>">
          <span class="fa fa-cubes"></span>
          Products
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link h6" href="#">
          <span class="fa fa-users"></span>
          Customers
        </a>
      </li>
      <li class="nav-item">
       <a class="nav-link h6 <?=$suppliers?>" href="index.php?page=<?=page_url('suppliers')?>">
          <span class="fa fa-truck"></span>
          Suppliers
        </a>
      </li>
    </ul>

    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>reports</span>
    </h6>
    <ul class="nav flex-column mb-2">
      <li class="nav-item">
        <a class="nav-link h6" href="#">
          <span class="fa fa-bar-chart"></span>
          Sales Report
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link h6" href="#">
          <span class="fa fa-archive"></span>
          Inventory Report
        </a>
      </li>
    </ul>
</div>